/*!
  @file Signal_Generation.h
  @brief Библиотека для расчета тестового сигнала
*/

#ifndef SIGNAL_GENERATION_H
#define SIGNAL_GENERATION_H

#include <QMainWindow>
#include "math.h"

/*!
  @fn void Null_uint16_t(uint16_t *signal_mas, uint32_t size_of_signal)
  @brief Функция обнуления массива uint16_t
  @param *signal_mas: - указатель на массив для обнуления
  @param size_of_signal: - размер массива (в байтах)
*/
void Null_uint16_t(uint16_t *signal_mas, uint32_t size_of_signal);

/*!
  @fn void Null_float(float *signal_mas, uint32_t size_of_signal)
  @brief Функция обнуления массива float
  @param *signal_mas: - указатель на массив для обнуления
  @param size_of_signal: - размер массива (в байтах)
*/
void Null_float(float *signal_mas, uint32_t size_of_signal);

/*!
  @fn void Sinus_Generation_16(uint16_t *signal_mas, uint32_t size_of_signal, uint32_t FREQ)
  @brief Функция расчета синуса
  @param *signal_mas: - указатель на массив для записи в него тестового сигнала
  @param size_of_signal: - размер массива (в байтах)
  @param FREQ: - частота сигнала (в Гц)
*/
void Sinus_Generation_16(uint16_t *signal_mas, uint32_t size_of_signal, uint32_t FREQ);

/*!
  @fn void GBO_Signal_Generation_16(uint16_t *signal_mas, uint32_t start_dot, uint32_t size_of_signal, uint32_t Um, float STEP, uint32_t FREQ)
  @brief Функция генерации тестового сигнала для ГБО
  @param *signal_mas: - указатель на массив для записи в него тестового сигнала
  @param start_dot: - номер элемента массива, когда значение сигнала принимает значение выше 0 (т.е. в момент времени возврата отраженного сигнала)
  @param size_of_signal: - размер массива (в байтах)
  @param Um: - напряжение сигнала первого периода
  @param STEP: - шаг дискретизации ЦАП
  @param FREQ: - частота сигнала (в Гц)
*/
void GBO_Signal_Generation_16(uint16_t *signal_mas, uint32_t start_dot, uint32_t size_of_signal, uint32_t Um, float STEP, uint32_t FREQ);

/*!
  @fn void GBO_Signal_Envelope(float *signal_mas, uint32_t start_dot, uint32_t size_of_signal, float Um, float STEP)
  @brief Функция создания огибающей для сигнала
  @param *signal_mas: - указатель на массив для записи в него тестового сигнала
  @param start_dot: - номер элемента массива, когда значение сигнала принимает значение выше 0 (т.е. в момент времени возврата отраженного сигнала)
  @param size_of_signal: - размер массива (в байтах)
  @param Um: - напряжение сигнала первого периода
  @param STEP: - шаг дискретизации ЦАП
*/
void GBO_Signal_Envelope(float *signal_mas, uint32_t start_dot, uint32_t size_of_signal, float Um, float STEP);

/*!
  @fn void GBO_Signal_Envelope_Log(float *signal_mas, uint32_t start_dot, uint32_t size_of_signal, float Um, float STEP)
  @brief Функция создания огибающей для сигнала в логарифмическом масштабе
  @param *signal_mas: - указатель на массив для записи в него тестового сигнала
  @param start_dot: - номер элемента массива, когда значение сигнала принимает значение выше 0 (т.е. в момент времени возврата отраженного сигнала)
  @param size_of_signal: - размер массива (в байтах)
  @param Um: - напряжение сигнала первого периода
  @param STEP: - шаг дискретизации ЦАП
*/
void GBO_Signal_Envelope_Log(float *signal_mas, uint32_t start_dot, uint32_t size_of_signal, float Um, float STEP);

/*!
  @fn void GBO_Signal_Evenlope_DIV(uint16_t *out_signal_mas, uint16_t nf, uint32_t start_dot, uint32_t size_of_signal, uint16_t Um, float STEP, uint32_t FREQ)
  @brief Функция, чтобы сделать огибающую сигнала, где каждая точка - период синусоиды
  @param *out_signal_mas: - указатель на массив для записи в него огибающей
  @param nf: - количество периодов, которые помещаются в сигнал
  @param start_dot: - номер элемента массива, когда значение сигнала принимает значение выше 0 (т.е. в момент времени возврата отраженного сигнала)
  @param size_of_signal: - размер массива (в байтах)
  @param Um: - напряжение сигнала первого периода
  @param STEP: - шаг дискретизации ЦАП
  @param FREQ: - частота сигнала (в Гц)
*/
void GBO_Signal_Evenlope_DIV(uint16_t *out_signal_mas, uint16_t nf, uint32_t start_dot, uint32_t size_of_signal, uint16_t Um, float STEP, uint32_t FREQ);


#endif // SIGNAL_GENERATION_H
