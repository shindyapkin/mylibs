#include "Signal_Generation.h"

//Функция обнуления массива uint16_t
void Null_uint16_t(uint16_t *signal_mas, uint32_t size_of_signal) {
    for(uint32_t i=0; i<size_of_signal;i++) {
        signal_mas[i]=1;
    }
}

//Функция обнуления массива float
void Null_float(float *signal_mas, uint32_t size_of_signal) {
    for(uint32_t i=0; i<size_of_signal;i++) {
        signal_mas[i]=0.1;
    }
}

//Функция генерации тестового сигнала для ГБО
void GBO_Signal_Generation_16(uint16_t *signal_mas, uint32_t start_dot, uint32_t size_of_signal, uint32_t Um, float STEP, uint32_t FREQ) {
    float AMPL1=Um*(pow((start_dot*STEP*750),2)/pow(10,(-7.5*STEP*start_dot)));
    float time_shift=0;

    //Обнулим массив
    Null_uint16_t(signal_mas, size_of_signal);

    signal_mas[start_dot-1]=5;
    signal_mas[start_dot]=signal_mas[start_dot-1]*1.3+1;

    uint16_t signal_mas_control=signal_mas[start_dot];

    //Если амплитуда Um не равна нулю, то сдвинем фазу
    if(Um>0) {
        //Чтобы сигнал не был "рваный", сделаем так, чтобы он начинался с того же значения,
        //что было в паузе (т.е. добавим угол сдвига фазы time_shift)
        while((signal_mas_control>=signal_mas[start_dot]) && ((signal_mas[start_dot]>(signal_mas[start_dot-1]*1.1)) || (signal_mas[start_dot]<(signal_mas[start_dot-1]*0.9)))) {
            float t=STEP*start_dot;
            float VALUE_SIN=(sin(2*3.1415*FREQ*t+time_shift)+1)/2;
            float TIME_CHANGE=pow(10,(-7.5*t))/pow((t*750),2);
            signal_mas[start_dot]=(AMPL1*VALUE_SIN*TIME_CHANGE);
            time_shift+=0.00001;
            signal_mas_control=signal_mas[start_dot];
        }
    }

    //Создаим массив с определенной точки
    for(uint32_t i=start_dot+1; i<size_of_signal; i++) {
        float t=i*STEP;
        float VALUE_SIN=(sin(2*3.1415*FREQ*t+time_shift)+1)/2;
        float TIME_CHANGE=pow(10,(-7.5*t))/pow((t*750),2);
        signal_mas[i]=(AMPL1*VALUE_SIN*TIME_CHANGE);
    }
}

//Функция создания огибающей для сигнала
void GBO_Signal_Envelope(float *signal_mas, uint32_t start_dot, uint32_t size_of_signal, float Um, float STEP) {
    float AMPL1=Um*(pow((start_dot*STEP*750),2)/pow(10,(-7.5*STEP*start_dot)));

    //Обнулим массив
    Null_float(signal_mas, size_of_signal);

    signal_mas[start_dot-1]=1;
    signal_mas[start_dot]=signal_mas[start_dot-1]*1.3;

    for(uint32_t i=start_dot+1; i<size_of_signal; i++) {
        float t=i*STEP;
        float TIME_CHANGE=pow(10,(-7.5*t))/pow((t*750),2);
        signal_mas[i]=(AMPL1*TIME_CHANGE);
    }
}

//Функция создания огибающей для сигнала в логарифмическом масштабе
void GBO_Signal_Envelope_Log(float *signal_mas, uint32_t start_dot, uint32_t size_of_signal, float Um, float STEP) {
    float AMPL1=Um*(pow((start_dot*STEP*750),2)/pow(10,(-7.5*STEP*start_dot)));

    //Обнулим массив
    Null_float(signal_mas, size_of_signal);

    signal_mas[start_dot-1]=1;
    signal_mas[start_dot]=signal_mas[start_dot-1]*1.3;

    for(uint32_t i=start_dot+1; i<size_of_signal; i++) {
        float t=i*STEP;
        float TIME_CHANGE=pow(10,(-7.5*t))/pow((t*750),2);
        signal_mas[i]=(AMPL1*TIME_CHANGE);
        signal_mas[i]=20*log10f(signal_mas[i]/0.000001);
    }
}

//Функция, чтобы сделать огибающую сигнала, где каждая точка - период синусоиды
void GBO_Signal_Evenlope_DIV(uint16_t *out_signal_mas, uint16_t nf, uint32_t start_dot, uint32_t size_of_signal, uint16_t Um, float STEP, uint32_t FREQ) {
    //Выделяем память под массив огибающей
    float *signal_mas_DIV=(float*)calloc(size_of_signal,4);

    //Посчитаем огибающую под амплитуду АЦП Um
    GBO_Signal_Envelope(signal_mas_DIV, start_dot, size_of_signal, Um, STEP);

    //Посчитаем через сколько точек надо брать значения периодов
    float k0 = 1/STEP/FREQ;

    //Теперь наберем наш массив
    for(uint32_t i=0; i<nf; i++) {
        uint32_t dot = (i*k0);
        out_signal_mas[i]=uint32_t(signal_mas_DIV[dot]);
    }

    //Освобождаем память
    free(signal_mas_DIV);
}
