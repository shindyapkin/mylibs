#ifndef SLIDER_SPINBOX_WIDGET_H
#define SLIDER_SPINBOX_WIDGET_H

#include <QWidget>
#include <math.h>

namespace Ui {
class Slider_SpinBox_Widget;
}

class Slider_SpinBox_Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Slider_SpinBox_Widget(QWidget *parent = nullptr);
    ~Slider_SpinBox_Widget();

    void setMinimum(uint32_t value);

    void setMaximum(uint32_t value);

    uint32_t getMaximum();

    void setText(QString text);

    void setStep(double step);

    void setTickInterval(int ticks);

    void setSpinBoxDecimals(int decimals);

    double getValue();

    void setValue(double value);

private slots:
    void on_horizontalSlider_valueChanged(int value);

    void on_doubleSpinBox_valueChanged(double arg1);

signals:
    void value_changed();

private:
    Ui::Slider_SpinBox_Widget *ui;
    double _step = 1;
    double _value = 0;
};

#endif // SLIDER_SPINBOX_WIDGET_H
