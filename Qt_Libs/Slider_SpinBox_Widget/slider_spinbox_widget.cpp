#include "slider_spinbox_widget.h"
#include "ui_slider_spinbox_widget.h"

Slider_SpinBox_Widget::Slider_SpinBox_Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Slider_SpinBox_Widget)
{
    ui->setupUi(this);

    _value = ui->doubleSpinBox->value();
}

Slider_SpinBox_Widget::~Slider_SpinBox_Widget()
{
    delete ui;
}

void Slider_SpinBox_Widget::setMinimum(uint32_t value)
{
    ui->doubleSpinBox->setMinimum(value * _step);
    ui->horizontalSlider->setMinimum(value);
}

void Slider_SpinBox_Widget::setMaximum(uint32_t value)
{
    ui->doubleSpinBox->setMaximum(value * _step);
    ui->horizontalSlider->setMaximum(value);
}

uint32_t Slider_SpinBox_Widget::getMaximum()
{
    return ui->horizontalSlider->maximum();
}

void Slider_SpinBox_Widget::setText(QString text)
{
    ui->text_label->setText(text);
}

void Slider_SpinBox_Widget::setStep(double step)
{
    _step = step;
    ui->doubleSpinBox->setSingleStep(step);
    ui->horizontalSlider->setSingleStep(step);
}

void Slider_SpinBox_Widget::setTickInterval(int ticks)
{
    ui->horizontalSlider->setTickInterval(ticks);
}

void Slider_SpinBox_Widget::setSpinBoxDecimals(int decimals)
{
    ui->doubleSpinBox->setDecimals(decimals);
}

double Slider_SpinBox_Widget::getValue()
{
    return _value;
}

void Slider_SpinBox_Widget::setValue(double value)
{
    _value = value;
    ui->doubleSpinBox->setValue(_value);
    ui->horizontalSlider->setValue(_value / _step);
}

void Slider_SpinBox_Widget::on_horizontalSlider_valueChanged(int value)
{
    _value = value * _step;
    ui->doubleSpinBox->setValue(_value);

    emit value_changed();
}

void Slider_SpinBox_Widget::on_doubleSpinBox_valueChanged(double arg1)
{
    int value = round(arg1 / _step);
    ui->horizontalSlider->setValue(value);
}
