#include "label_edit_button_widget.h"
#include "ui_label_edit_button_widget.h"

Label_Edit_Button_Widget::Label_Edit_Button_Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Label_Edit_Button_Widget)
{
    ui->setupUi(this);
}

Label_Edit_Button_Widget::~Label_Edit_Button_Widget()
{
    delete ui;
}

void Label_Edit_Button_Widget::setLabelText(QString text)
{
    ui->label->setText(text);
}

void Label_Edit_Button_Widget::setButtonText(QString text)
{
    ui->pushButton->setText(text);
}

void Label_Edit_Button_Widget::setEditText(QString text)
{
    ui->lineEdit->setText(text);
}

QString Label_Edit_Button_Widget::getEditText()
{
    return ui->lineEdit->text();
}

void Label_Edit_Button_Widget::on_pushButton_clicked()
{
    emit button_clicked();
}
