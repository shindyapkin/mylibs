#ifndef LABEL_EDIT_BUTTON_WIDGET_H
#define LABEL_EDIT_BUTTON_WIDGET_H

#include <QWidget>

namespace Ui {
class Label_Edit_Button_Widget;
}

class Label_Edit_Button_Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Label_Edit_Button_Widget(QWidget *parent = nullptr);
    ~Label_Edit_Button_Widget();

    void setLabelText(QString text);

    void setButtonText(QString text);

    void setEditText(QString text);

    QString getEditText();

private slots:
    void on_pushButton_clicked();

signals:
    void button_clicked();

private:
    Ui::Label_Edit_Button_Widget *ui;
};

#endif // LABEL_EDIT_BUTTON_WIDGET_H
