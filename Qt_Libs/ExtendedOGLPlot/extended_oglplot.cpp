#include "extended_oglplot.h"
#include "ui_extended_oglplot.h"

extended_oglplot::extended_oglplot(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::extended_oglplot)
{
    ui->setupUi(this);
//    this->setGeometry(parent->geometry());

//    QSize size = ui->plot_widget->size();//ui->plot_widget->geometry().size();
//    chart = new OGLPlot(0, 0, ui->plot_widget->width(),  ui->plot_widget->height(), ui->plot_widget);


    ui->plot_horizontalScrollBar->setMinimum(0);
    ui->plot_horizontalScrollBar->setMaximum(0);
    ui->plot_horizontalScrollBar->setValue(0);

    ui->plot_verticalScrollBar->setMinimum(0);
    ui->plot_verticalScrollBar->setMaximum(0);
    ui->plot_verticalScrollBar->setValue(0);

//    chart->setPlotSize(0, 0, ui->plot_widget->width(),  ui->plot_widget->height());
//    connect(this, SIGNAL(resize(QSize *)), ui->plot_widget, SIGNAL(resize(QSize *)));
}

extended_oglplot::~extended_oglplot()
{
    delete ui;
}

void extended_oglplot::setStartSizes(double min_x, double max_x, double min_y, double max_y)
{
    min_x_value = min_x;
    max_x_value = max_x;
    min_y_value = min_y;
    max_y_value = max_y;

    ui->min_x_value_doubleSpinBox->setMinimum(min_x);
    ui->min_x_value_doubleSpinBox->setMaximum(max_x);
    ui->min_x_value_doubleSpinBox->setValue(min_x);

    ui->max_x_value_doubleSpinBox->setMinimum(min_x);
    ui->max_x_value_doubleSpinBox->setMaximum(max_x);
    ui->max_x_value_doubleSpinBox->setValue(max_x);

    ui->min_y_value_doubleSpinBox->setMinimum(min_y);
    ui->min_y_value_doubleSpinBox->setMaximum(max_y);
    ui->min_y_value_doubleSpinBox->setValue(min_y);

    ui->max_y_value_doubleSpinBox->setMinimum(min_y);
    ui->max_y_value_doubleSpinBox->setMaximum(max_y);
    ui->max_y_value_doubleSpinBox->setValue(max_y);

    if(new_current_value) {
        enter_mode = 0;
        ui->count_of_dots_doubleSpinBox->setValue(max_x - min_x);
        ui->amplitude_doubleSpinBox->setValue(max_y - min_y);
        new_current_value = 0;
    }

    double dif_x = max_x - min_x;
    double dif_y = max_y - min_y;
    ui->count_of_dots_doubleSpinBox->setMaximum(dif_x);
    if(count_of_dots == 0) {
        ui->count_of_dots_doubleSpinBox->setValue(dif_x);
    }
    ui->amplitude_doubleSpinBox->setMaximum(dif_y);
    if(amplitude == 0) {
        ui->amplitude_doubleSpinBox->setValue(dif_y);
    }

    enter_mode = 1;
}

void extended_oglplot::setAxisMultipliers(float stepX, float stepY)
{
    ui->plot_widget->setAxisMultipliers(stepX, stepY);
}

void extended_oglplot::switchParametersPanel()
{
    ui->parameters_toolButton->click();
}

void extended_oglplot::setPlotParameters()
{
    uint32_t pos_x = ui->plot_horizontalScrollBar->value();
    uint32_t pos_y = ui->plot_verticalScrollBar->value();

    min_x_value = ui->min_x_value_doubleSpinBox->value();
    max_x_value = ui->max_x_value_doubleSpinBox->value();
    min_y_value = ui->min_y_value_doubleSpinBox->value();
    max_y_value = ui->max_y_value_doubleSpinBox->value();
    count_of_dots = ui->count_of_dots_doubleSpinBox->value();
    amplitude = ui->amplitude_doubleSpinBox->value();

    ui->min_x_value_doubleSpinBox->setMaximum(max_x_value - min_x_value);
    ui->count_of_dots_doubleSpinBox->setMaximum(max_x_value - min_x_value);

    ui->min_y_value_doubleSpinBox->setMaximum(max_y_value);
    ui->amplitude_doubleSpinBox->setMaximum(max_y_value);

    double dif_x = (max_x_value - min_x_value) - count_of_dots;
    double dif_y = (max_y_value - min_y_value) - amplitude;

    if(dif_x > 0) {
        ui->plot_horizontalScrollBar->setMaximum(dif_x);
        ui->plot_horizontalScrollBar->setMinimum(0);
    } else {
        ui->plot_horizontalScrollBar->setMaximum(0);
        ui->plot_horizontalScrollBar->setMinimum(0);
    }

    if(dif_y > 0) {
        ui->plot_verticalScrollBar->setMaximum(dif_y);
        ui->plot_verticalScrollBar->setMinimum(0);
    } else {
        ui->plot_verticalScrollBar->setMaximum(0);
        ui->plot_verticalScrollBar->setMinimum(0);
    }

    if(enter_mode == 1) {
        ui->plot_widget->plot<double, double>(chart_data, pos_x + min_x_value, pos_x + count_of_dots + min_x_value, pos_y + min_y_value, pos_y + amplitude + min_y_value);
    }
}

void extended_oglplot::on_plot_horizontalScrollBar_valueChanged(int value)
{
    setPlotParameters();
}

void extended_oglplot::on_plot_verticalScrollBar_valueChanged(int value)
{
    setPlotParameters();
}

void extended_oglplot::on_min_x_value_doubleSpinBox_valueChanged(double arg1)
{
    setPlotParameters();
}

void extended_oglplot::on_max_x_value_doubleSpinBox_valueChanged(double arg1)
{
    setPlotParameters();
}

void extended_oglplot::on_min_y_value_doubleSpinBox_valueChanged(double arg1)
{
    setPlotParameters();
}

void extended_oglplot::on_max_y_value_doubleSpinBox_valueChanged(double arg1)
{
    setPlotParameters();
}

void extended_oglplot::on_count_of_dots_doubleSpinBox_valueChanged(double arg1)
{
    setPlotParameters();
}

void extended_oglplot::on_amplitude_doubleSpinBox_valueChanged(double arg1)
{
    setPlotParameters();
}

void extended_oglplot::on_pushButton_clicked()
{
    double min_x = ui->min_x_value_doubleSpinBox->minimum();
    double max_x = ui->max_x_value_doubleSpinBox->maximum();
    double min_y = ui->min_y_value_doubleSpinBox->minimum();
    double max_y = ui->max_y_value_doubleSpinBox->maximum();
    setStartSizes(min_x, max_x, min_y, max_y);
    enter_mode = 0;
    ui->count_of_dots_doubleSpinBox->setValue(max_x - min_x);
    ui->amplitude_doubleSpinBox->setValue(max_y - min_y);
    enter_mode = 1;
    setPlotParameters();
}

void extended_oglplot::on_parameters_toolButton_clicked(bool checked)
{
    int size_inc = ui->plot_parameters_groupBox->height() + ui->plot_horizontalScrollBar->height();
    if(!checked) {
        size_inc *= (-1.0f);
    }

    ui->plot_parameters_groupBox->setHidden(checked);
}
