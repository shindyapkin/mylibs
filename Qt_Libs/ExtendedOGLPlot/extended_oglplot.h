#ifndef EXTENDED_OGLPLOT_H
#define EXTENDED_OGLPLOT_H

#include <QWidget>
#include "oglplot.h"

namespace Ui {
class extended_oglplot;
}

struct Slider {
    uint32_t min_val = 0;
    uint32_t max_val = 0;
    uint32_t current_val = 0;
};

class extended_oglplot : public QWidget
{
    Q_OBJECT

public:
    explicit extended_oglplot(QWidget *parent = nullptr);
    ~extended_oglplot();

    void setAxisMultipliers(float stepX, float stepY);

    template<typename Type1, typename Type2>
    void plot(QVector<Type1> data, Type2 min_x, Type2 max_x, Type1 min_y, Type1 max_y) {
        if(chart_data.size() > 0) {
            chart_data.clear();
        }
        chart_data = data;

        new_current_value = 1;
        setStartSizes(min_x, max_x, min_y, max_y);
        setPlotParameters();
    }

    template<typename Type1, typename Type2>
    void plot(Type1 data[], int size_of_data, Type2 min_x, Type2 max_x, Type1 min_y, Type1 max_y) {
        chart_data.clear();
        for(int i = 0; i < size_of_data; i++) {
            chart_data.push_back(data[i]);
        }

        new_current_value = 1;
//        enter_mode = 0;
        setStartSizes(min_x, max_x, min_y, max_y);
//        enter_mode = 1;
        setPlotParameters();
    }

    void switchParametersPanel();

private slots:
    void on_plot_horizontalScrollBar_valueChanged(int value);

    void on_plot_verticalScrollBar_valueChanged(int value);

    void on_min_x_value_doubleSpinBox_valueChanged(double arg1);

    void on_max_x_value_doubleSpinBox_valueChanged(double arg1);

    void on_min_y_value_doubleSpinBox_valueChanged(double arg1);

    void on_max_y_value_doubleSpinBox_valueChanged(double arg1);

    void on_count_of_dots_doubleSpinBox_valueChanged(double arg1);

    void on_amplitude_doubleSpinBox_valueChanged(double arg1);

    void on_pushButton_clicked();

    void on_parameters_toolButton_clicked(bool checked);


//public slots:
//         virtual void resizeEvent(QResizeEvent *event) override;

private:
    Ui::extended_oglplot *ui;
    OGLPlot *chart;
    QVector<double> chart_data;
    double min_x_value = 0;
    double max_x_value = 0;
    double min_y_value = 0;
    double max_y_value = 0;
    uint32_t count_of_dots = 0;
    double amplitude = 0;

    uint8_t enter_mode = 0;
    uint8_t new_current_value = 0;

    void setStartSizes(double min_x, double max_x, double min_y, double max_y);

    void setPlotParameters();


};

#endif // EXTENDED_OGLPLOT_H
