#ifndef CHARTPAINT_H
#define CHARTPAINT_H

#include <QtCharts/QtCharts>

typedef enum
{
    AXIS_NORM = 1,
    AXIS_LOG = 0,
}TYPE_OF_AXIS;

/*!
    @class ChartPaint
    @brief Класс, описывающий работу с объектом QChart
 */
class ChartPaint {
public:
    /*!
      @fn ChartPaint(QVBoxLayout *Layout)
      @brief Функция создания графика
      @param *Layout: - объект Vertical Layout, на котором будет размещаться график
    */
    ChartPaint(QVBoxLayout *Layout);

    ~ChartPaint() = default;

    /*!
      @fn void SetFloatAxis(QLineSeries *series, QValueAxis *axis, QString NameAxis, Qt::Alignment alignment, qreal min, qreal max, float step)
      @brief Функция настройки оси
      @param *series: - данные, помещаемые в график
      @param *axis: - ось графика
      @param NameAxis: - подпись оси
      @param aligment: - сторона размещения оси
      @param min: - минимальное значение на оси
      @param max: - максимальное значение на оси
      @param step: - шаг оси
    */
    void SetFloatAxis(QLineSeries *series, QValueAxis *axis, QString NameAxis, Qt::Alignment alignment, qreal min, qreal max, float step);

    /*!
      @fn void SetFloatLogAxis(QLineSeries *series, QLogValueAxis *axis, QString NameAxis, Qt::Alignment alignment, qreal min, qreal max, float step)
      @brief Функция настройки логарифмической оси
      @param *series: - данные, помещаемые в график
      @param *axis: - ось графика
      @param NameAxis: - подпись оси
      @param aligment: - сторона размещения оси
      @param min: - минимальное значение на оси
      @param max: - максимальное значение на оси
      @param step: - шаг оси
    */
    void SetFloatLogAxis(QLineSeries *series, QLogValueAxis *axis, QString NameAxis, Qt::Alignment alignment, qreal min, qreal max, float step);

    /*!
      @fn ChartAppend(unsigned short *osc_data, uint32_t size, qreal minX, qreal minY, qreal maxY, uint8_t Type_Of_AxisX, uint8_t Type_Of_AxisY)
      @brief Функция добавления данных в график формата uint16_t
      @param *osc_data: - указатель на массив с данными, которые будут отрисованы на графике
      @param size: - размер массива
      @param minX: - минимальное значение на оси X
      @param maxY: - максимальное значение на оси Y
      @param Type_Of_AxisX: - тип оси Х (линейный или логарифмический)
      @param Type_Of_AxisY: - тип оси Y (линейный или логарифмический)
    */
    void ChartAppend(unsigned short *osc_data, uint32_t size, qreal minX, qreal minY, qreal maxY, uint8_t Type_Of_AxisX, uint8_t Type_Of_AxisY);

    void ChartAdd(unsigned short *osc_data, uint32_t size, qreal minX, qreal minY, qreal maxY, uint8_t Type_Of_AxisX, uint8_t Type_Of_AxisY);

    /*!
      @fn void ChartAppendFloat(float *osc_data, uint32_t size, qreal minX, qreal minY, qreal maxY, uint8_t Type_Of_AxisX, uint8_t Type_Of_AxisY)
      @brief Функция добавленя данных в график формата float
      @param *osc_data: - указатель на массив с данными, которые будут отрисованы на графике
      @param size: - размер массива
      @param minX: - минимальное значение на оси X
      @param maxY: - максимальное значение на оси Y
      @param Type_Of_AxisX: - тип оси Х (линейный или логарифмический)
      @param Type_Of_AxisY: - тип оси Y (линейный или логарифмический)
    */
    void ChartAppendFloat(float *osc_data, uint32_t size, qreal minX, qreal minY, qreal maxY, uint8_t Type_Of_AxisX, uint8_t Type_Of_AxisY);

    /*!
      @fn void SetAxisText(QString textX, QString textY)
      @brief Функция, чтобы задать текст на подпись осей
      @param textX: - подпись оси Х
      @param textY: - подпись оси Y
    */
    void SetAxisText(QString textX, QString textY);

    /*!
      @fn void SetStepAxis(qreal stepX, qreal stepY)
      @brief Функция, чтобы задать шаг оси
      @param stepX: - шаг для оси X
      @param stepY: - шаг для оси Y
    */
    void SetStepAxis(qreal stepX, qreal stepY);

private:
    /*!
      @var *chart
      @brief Объект графика
    */
//    QChart *chart = new QChart();
//    QLineSeries *series = new QLineSeries();
//    std::unique_ptr<QChart> chart;
//    std::shared_ptr<QLineSeries> series;
//    std::unique_ptr<QChartView> chartView;
    QChart *chart;
    QLineSeries *series;
    QChartView *chartView;

    /*!
      @var *axisX
      @brief Объект оси Х
    */
    QValueAxis *axisX;

    /*!
      @var *axisY
      @brief Объект оси Y
    */
    QValueAxis *axisY;

    /*!
      @var *log_axisX
      @brief Объект логарифмической оси X
    */
    QLogValueAxis *log_axisX;

    /*!
      @var *log_axisY
      @brief Объект логарифмической оси Y
    */
    QLogValueAxis *log_axisY;

    /*!
      @var stringX
      @brief Переменная текста для оси X
    */
    QString stringX="";

    /*!
      @var stringY
      @brief Переменная текста для оси Y
    */
    QString stringY="";

    /*!
      @var step_X
      @brief Шаг оси X
    */
    qreal step_X = 1;

    /*!
      @var step_Y
      @brief Шаг оси Y
    */
    qreal step_Y = 1;

    qreal TIM_PER = 0.0000002;
};

#endif // CHARTPAINT_H
