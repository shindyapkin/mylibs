#include "ChartPaint.h"

//Создаем график
ChartPaint::ChartPaint(QVBoxLayout *Layout) {
//    chart = std::make_unique<QChart>();
//    series = std::make_shared<QLineSeries>();

    //Объявим наш Chart
//    chartView = std::make_unique<QChartView>(chart.get());
    chart = new QChart;
    series = new QLineSeries;
    chartView = new QChartView(chart);

    axisX = new QValueAxis();
    axisY = new QValueAxis();
    log_axisX = new QLogValueAxis();
    log_axisY = new QLogValueAxis();

    //Выбираем его тип
    chartView->setRenderHint(QPainter::Antialiasing);



    series->setUseOpenGL(true);
    series->useOpenGL();

    //Размещаем его на нужном слое
    Layout->addWidget(chartView);

    chart->legend()->hide();
}

//Функция настройки осей
void ChartPaint::SetFloatAxis(QLineSeries *series, QValueAxis *axis, QString NameAxis, Qt::Alignment alignment, qreal min, qreal max, float step) {
    //Устанавливаем текст на подпись оси
    axis->setTitleText(NameAxis);

    //Выбираем тип данных, который будет располоден на оси
    axis->setLabelFormat("%.3f");

    //Зададим масштаб оси
    axis->setRange(min, max);

    //Зададим шаг оси
    axis->setTickInterval(step);

    //Зададим край выравнивания для оси
    chart->addAxis(axis, alignment);
    series->attachAxis(axis);
}

//Функция настройки логарифмических осей
void ChartPaint::SetFloatLogAxis(QLineSeries *series, QLogValueAxis *axis, QString NameAxis, Qt::Alignment alignment, qreal min, qreal max, float step) {
    //Устанавливаем текст на подпись оси
    axis->setTitleText(NameAxis);

    //Выбираем тип данных, который будет располоден на оси
    axis->setLabelFormat("%.3f");

    axis->setMin(min+step);
    axis->setMax(max);

    //Зададим масштаб оси
    axis->setBase(1);

    //Зададим шаг оси
    axis->setMinorTickCount(-1);

    //Зададим край выравнивания для оси
    chart->addAxis(axis, alignment);
    series->attachAxis(axis);
}

//Функция, чтобы задать текст на подпись осей
void ChartPaint::SetAxisText(QString textX, QString textY) {
    stringX=textX;
    stringY=textY;
}

//Функция, чтобы задать шаг оси
void ChartPaint::SetStepAxis(qreal stepX, qreal stepY) {
    step_X=stepX;
    step_Y=stepY;
}

//Добавляем данные в график
void ChartPaint::ChartAppend(unsigned short *osc_data, uint32_t size, qreal minX, qreal minY, qreal maxY, uint8_t Type_Of_AxisX, uint8_t Type_Of_AxisY) {
    //Очищаем нашу серию
    series->clear();

    //Добавляем данные из osc_data в QLineSeries
    for(uint32_t i = (uint32_t)minX; i < size+(uint32_t)minX; i++) {
        series->append(i * step_X, osc_data[i] * step_Y);
    }

    //Очистим график
    if(!chart->series().empty()) {
        for(auto ser : chart->series()) {
           chart->removeSeries(ser);
        }
    }
    chart->removeAxis(axisX);
    chart->removeAxis(axisY);
    chart->removeAxis(log_axisX);
    chart->removeAxis(log_axisY);

    //Выводим данные в график
    chart->addSeries(series);

    //В зависимости от заданного типа, построим тип оси Х
    switch(Type_Of_AxisX) {

        case AXIS_NORM:
            //Создаем обычную ось X
//            SetFloatAxis(series.get(), axisX, stringX, Qt::AlignBottom, step_X * minX, step_X * (size - step_X + minX), step_X);
            SetFloatAxis(series, axisX, stringX, Qt::AlignBottom, step_X * minX, step_X * (size - step_X + minX), step_X);
        break;

        case AXIS_LOG:
            //Создаем логарифмическую ось X
//            SetFloatLogAxis(series.get(), log_axisX, stringX, Qt::AlignBottom, step_X * minX, step_X * (size + minX), step_X);
            SetFloatLogAxis(series, log_axisX, stringX, Qt::AlignBottom, step_X * minX, step_X * (size + minX), step_X);
        break;
    }
    //В зависимости от заданного типа, построим тип оси Y
    switch(Type_Of_AxisY) {

        case AXIS_NORM:
            //Создаем обычную ось Y
//           SetFloatAxis(series.get(), axisY, stringY, Qt::AlignLeft, minY, maxY , step_Y);
           SetFloatAxis(series, axisY, stringY, Qt::AlignLeft, minY, maxY , step_Y);
        break;

        case AXIS_LOG:
            //Создаем логарифмическую ось Y
//            SetFloatLogAxis(series.get(), log_axisY, stringY, Qt::AlignLeft, minY, maxY, step_Y);
            SetFloatLogAxis(series, log_axisY, stringY, Qt::AlignLeft, minY, maxY, step_Y);
        break;
    }
}

//Добавляем данные в график
void ChartPaint::ChartAdd(unsigned short *osc_data, uint32_t size, qreal minX, qreal minY, qreal maxY, uint8_t Type_Of_AxisX, uint8_t Type_Of_AxisY) {
    QLineSeries *new_series = new QLineSeries;

    //Добавляем данные из osc_data в QLineSeries
    for(uint32_t i = (uint32_t)minX; i < size+(uint32_t)minX; i++) {
        new_series->append(i * step_X, osc_data[i] * step_Y);
    }

    //Вывод данные в график
    chart->addSeries(new_series);

    qreal maxX = step_X * (size - step_X + minX);
    if(maxX < axisX->max()) {
        maxX = axisX->max();
    }
    if(maxY < axisY->max()) {
        maxY = axisY->max();
    }

    //В зависимости от заданного типа, построим тип оси Х
    switch(Type_Of_AxisX) {

        case AXIS_NORM:
            //Создаем обычную ось X
            SetFloatAxis(new_series, axisX, stringX, Qt::AlignBottom, step_X * minX, maxX, step_X);
        break;

        case AXIS_LOG:
            //Создаем логарифмическую ось X
            SetFloatLogAxis(new_series, log_axisX, stringX, Qt::AlignBottom, step_X * minX, step_X * (size + minX), step_X);
        break;
    }
    //В зависимости от заданного типа, построим тип оси Y
    switch(Type_Of_AxisY) {

        case AXIS_NORM:
            //Создаем обычную ось Y
           SetFloatAxis(new_series, axisY, stringY, Qt::AlignLeft, minY, maxY , step_Y);
        break;

        case AXIS_LOG:
            //Создаем логарифмическую ось Y
            SetFloatLogAxis(new_series, log_axisY, stringY, Qt::AlignLeft, minY, maxY, step_Y);
        break;
    }
}

//Функция добавленя данных в график формата float
void ChartPaint::ChartAppendFloat(float *osc_data, uint32_t size, qreal minX, qreal minY, qreal maxY, uint8_t Type_Of_AxisX, uint8_t Type_Of_AxisY) {
    QLineSeries *series = new QLineSeries();

    series->setUseOpenGL(true);
    series->useOpenGL();

    //Очищаем нашу серию
    series->clear();

    //Добавляем данные из osc_data в QLineSeries
    for(uint32_t i=(uint32_t)minX; i<size+(uint32_t)minX; i++) {
        series->append(i*step_X+step_X, osc_data[i]  * step_Y);
    }

    //Очистим график
    chart->removeAllSeries();
    chart->removeAxis(axisX);
    chart->removeAxis(axisY);
    chart->removeAxis(log_axisX);
    chart->removeAxis(log_axisY);

    //В зависимости от заданного типа, построим тип оси Х
    switch(Type_Of_AxisX) {

        case AXIS_NORM:
            //Вывод данные в график
            chart->addSeries(series);

            //Создаем обычную ось X
            SetFloatAxis(series, axisX, stringX, Qt::AlignBottom, step_X*minX, step_X*(size+minX), step_X);
        break;

        case AXIS_LOG:
            //Выводим данные в график
            chart->addSeries(series);

            //Создаем логарифмическую ось X
            SetFloatLogAxis(series, log_axisX, stringX, Qt::AlignBottom, step_X*minX, step_X*(size+minX), step_X);
        break;
    }

    //В зависимости от заданного типа, построим тип оси Х
    switch(Type_Of_AxisY) {

        case AXIS_NORM:
            //Вывод данные в график
            chart->addSeries(series);

            //Создаем обычную ось Y
           SetFloatAxis(series, axisY, stringY, Qt::AlignLeft, minY, maxY , step_Y);
        break;

        case AXIS_LOG:
            //Выводим данные в график
            chart->addSeries(series);

            //Создаем логарифмическую ось Y
            SetFloatLogAxis(series, log_axisY, stringY, Qt::AlignLeft, minY, maxY, step_Y);
        break;
    }
}
