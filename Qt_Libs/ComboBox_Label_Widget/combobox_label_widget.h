#ifndef COMBOBOX_LABEL_WIDGET_H
#define COMBOBOX_LABEL_WIDGET_H

#include <QWidget>

namespace Ui {
class ComboBox_Label_Widget;
}

class ComboBox_Label_Widget : public QWidget
{
    Q_OBJECT

public:
    explicit ComboBox_Label_Widget(QWidget *parent = nullptr);
    ~ComboBox_Label_Widget();

    void setText(QString text);

    void addField(QString text);

    QString getCurrentField();


private:
    Ui::ComboBox_Label_Widget *ui;

signals:
    void value_changed();
private slots:
    void on_comboBox_currentIndexChanged(int index);
};

#endif // COMBOBOX_LABEL_WIDGET_H
