#include "combobox_label_widget.h"
#include "ui_combobox_label_widget.h"

ComboBox_Label_Widget::ComboBox_Label_Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ComboBox_Label_Widget)
{
    ui->setupUi(this);
}

ComboBox_Label_Widget::~ComboBox_Label_Widget()
{
    delete ui;
}

void ComboBox_Label_Widget::setText(QString text)
{
    ui->label->setText(text);
}

void ComboBox_Label_Widget::addField(QString text)
{
    ui->comboBox->addItem(text);
}

QString ComboBox_Label_Widget::getCurrentField()
{
    return ui->comboBox->currentText();
}

void ComboBox_Label_Widget::on_comboBox_currentIndexChanged(int index)
{
    emit value_changed();
}
