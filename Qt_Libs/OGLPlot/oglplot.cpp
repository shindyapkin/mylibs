#include "oglplot.h"


OGLPlot::OGLPlot(int x,int y, int width, int height, QWidget *parent) : QOpenGLWidget(parent)
{
    _x = x;
    _y = y;
    _width = width;
    _height = height;

    int horizontal_tab = width / 20;
    int vert_tab = height / 20;

    setFrame(horizontal_tab, horizontal_tab, vert_tab, vert_tab);
    setGrid(3, 3);
}

void OGLPlot::clearAllSeries()
{
    _draw_data.clear();
    this->update();
}

void OGLPlot::setAxisMultipliers(float stepX, float stepY)
{
    _stepX = stepX;
    _stepY = stepY;
}

void OGLPlot::resizeEvent(QResizeEvent *e)
{
    this->setGeometry(_x, _y, _width, _height);
}

void OGLPlot::initializeGL() {
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, _width, _height, 0, 0, 1);
}

void OGLPlot::resizeGL(int w, int h) {
    glViewport(0, 0, (GLint)w, (GLint)h);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
//    glOrtho( 0, w, h, 0, -1, +1 );
}

void OGLPlot::paintGL() {
    //Чистим поле
    glMatrixMode(GL_PROJECTION);
    glClearColor(1, 1, 1, 0.1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //Устанавливаем сглаживание
    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    glHint(GL_LINE_SMOOTH_HINT,GL_NICEST);

    //Рисуем сетку
    glColor3f(0.8, 0.8, 0.8);
    glLineWidth(2);
    glBegin(GL_LINES);
    {
        for(int i = 0; i < _hor_grid_lines.x.size(); i++) {
            glVertex2i(_hor_grid_lines.x[i], _hor_grid_lines.y_low);
            glVertex2i(_hor_grid_lines.x[i], _hor_grid_lines.y_up);
        }

        for(int j = 0; j < _vert_grid_lines.y.size(); j++) {
            glVertex2i(_vert_grid_lines.x_left, _vert_grid_lines.y[j]);
            glVertex2i(_vert_grid_lines.x_right, _vert_grid_lines.y[j]);
        }
    }
    glEnd();

    //Чертим графики
    for(int k = 0; k < _draw_data.size(); k++) {
        glColor4ub(_draw_data[k].line_color.red(), _draw_data[k].line_color.green(), _draw_data[k].line_color.blue(), _draw_data[k].line_color.alpha());
        glLineWidth(_draw_data[k].line_width);
        glBegin(GL_LINE_STRIP);
        {
            for(int t = 0; t < _draw_data[k].data_coord.size(); t++) {
                glVertex2i(_draw_data[k].data_coord[t].first.toInt(), _draw_data[k].data_coord[t].second);
            }
        }
        glEnd();
    }

    //Рисуем рамку
    glColor3f(0.2, 0.2, 0.2);
    glLineWidth(3);
    glBegin(GL_LINE_LOOP);
    {
        glVertex2i(_frame_sizes[0].first, _frame_sizes[0].second);
        glVertex2i(_frame_sizes[1].first, _frame_sizes[1].second);
        glVertex2i(_frame_sizes[2].first, _frame_sizes[2].second);
        glVertex2i(_frame_sizes[3].first, _frame_sizes[3].second);
    }
    glEnd();

    if(_draw_data.size() > 0) {
        //Подписываем значения на осях
        QPainter painter(this);
//        painter.scale(1.0, -1.0);
        painter.setPen(QColor::fromRgb(166, 166, 166));
        painter.setFont(QFont("Comic Sans MS", grid_text_size, QFont::Medium));
        painter.setRenderHint(QPainter::Antialiasing);
        //Для начальных значений
        painter.drawText(_frame_sizes[3].first, _frame_sizes[3].second + grid_text_size + grid_text_size / 3, QString::number(_draw_data[_draw_data.size() - 1].min_x  * _stepX));
        QRect rect_y_top(_frame_sizes[3].first - QString::number(_draw_data[_draw_data.size() - 1].max_y).size() * (grid_text_size + 1) - 3, _frame_sizes[3].second - grid_text_size, QString::number(_draw_data[_draw_data.size() - 1].max_y).size() * (grid_text_size + 1), grid_text_size * 2);
        painter.drawText(rect_y_top, Qt::AlignRight | Qt::AlignTop, QString::number(_draw_data[_draw_data.size() - 1].min_y));
        //Для промежуточных значений
        //По оси Х
        for(int x = 0; x < _hor_grid_lines.x.size(); x++) {
            float number = (((_draw_data[_draw_data.size() - 1].max_x - _draw_data[_draw_data.size() - 1].min_x) * (x + 1)) / (_hor_grid_lines.x.size() + 1) + _draw_data[_draw_data.size() - 1].min_x) * _stepX;
            painter.drawText(_hor_grid_lines.x[x] - QString::number(number).size() * 4 * grid_text_size / 10, _hor_grid_lines.y_low + grid_text_size + 4, QString::number(number));
        }
        //По оси У
        for(int y = 0; y < _vert_grid_lines.y.size(); y++) {
            float number = (_draw_data[_draw_data.size() - 1].max_y - _draw_data[_draw_data.size() - 1].min_y) * (_vert_grid_lines.y.size() - y) / (_vert_grid_lines.y.size() + 1) + _draw_data[_draw_data.size() - 1].min_y;
            QRect rect_y(_frame_sizes[0].first - QString::number(_draw_data[_draw_data.size() - 1].max_y).size() * (grid_text_size + 1) - 3, _vert_grid_lines.y[y] - grid_text_size, QString::number(_draw_data[_draw_data.size() - 1].max_y).size() * (grid_text_size + 1), grid_text_size * 2);
            painter.drawText(rect_y, Qt::AlignRight | Qt::AlignTop, QString::number(number));
        }
        //Для конечных значений
        painter.drawText(_frame_sizes[2].first - QString::number(_draw_data[_draw_data.size() - 1].max_x).size() * 3 * grid_text_size / 4, _frame_sizes[2].second + grid_text_size + 4, QString::number(_draw_data[_draw_data.size() - 1].max_x * _stepX));
        QRect rect_y_bot(_frame_sizes[0].first - QString::number(_draw_data[_draw_data.size() - 1].max_y).size() * (grid_text_size + 1) - 3, _frame_sizes[0].second - grid_text_size, QString::number(_draw_data[_draw_data.size() - 1].max_y).size() * (grid_text_size + 1), grid_text_size * 2);
        painter.drawText(rect_y_bot, Qt::AlignRight | Qt::AlignTop, QString::number(_draw_data[_draw_data.size() - 1].max_y));
        painter.end();
    }
}

void OGLPlot::setFrame(int left_tab, int right_tab, int upper_tab, int bottom_tab) {
    //Задаем отступ рамок относительно наименьшего значения длины или ширины
/*    int frame_offset = 10;
    if(_width < _height) {
        frame_offset = _width / 20;
    } else {
        frame_offset = _height / 20;
    }*/

    _frame_sizes[0].first = left_tab;
    _frame_sizes[0].second = upper_tab;

    _frame_sizes[1].first = _width - right_tab;
    _frame_sizes[1].second = upper_tab;

    _frame_sizes[2].first = _width - right_tab;
    _frame_sizes[2].second = _height - bottom_tab;

    _frame_sizes[3].first = left_tab;
    _frame_sizes[3].second = _height - bottom_tab;

    _frame_width =  _frame_sizes[1].first - _frame_sizes[0].first;
    _frame_height = _frame_sizes[3].second - _frame_sizes[0].second;
}

void OGLPlot::setGrid(int hor_grid_lines_count, int vert_grid_lines_count)
{
    //Если у нас уже есть сетка, то чистим ее
    _hor_grid_lines.x.clear();
    _vert_grid_lines.y.clear();

    //Определяем точки для горизонтальных линий сетки
    _hor_grid_lines.y_up = _frame_sizes[0].second;
    _hor_grid_lines.y_low = _frame_sizes[3].second;
    for(int i = 1; i <= hor_grid_lines_count; i++) {
        _hor_grid_lines.x.push_back(_frame_sizes[0].first + _frame_width / (hor_grid_lines_count + 1) * i);
    }

    //Определяем точки для вертикальных линий сетки
    _vert_grid_lines.x_left = _frame_sizes[0].first;
    _vert_grid_lines.x_right = _frame_sizes[1].first;
    for(int j = 1; j <= vert_grid_lines_count; j++) {
        _vert_grid_lines.y.push_back(_frame_sizes[0].second + _frame_height / (hor_grid_lines_count + 1) * j);
    }
}
