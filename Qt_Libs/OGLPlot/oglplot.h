#ifndef OGLPLOT_H
#define OGLPLOT_H

#include <QtOpenGL>
#include <QWidget>
#include <QGLWidget>
#include <QOpenGLWidget>
#include <QPainter>
#include <QOpenGLFunctions>
#include <iostream>

class OGLPlot : QOpenGLWidget
{
public:
    OGLPlot(int x,int y, int width, int height, QWidget *parent = nullptr);

    //Метод добавления на график новой серии
    template<typename Type1, typename Type2>
    void append(QVector<Type1> data, Type2 min_x, Type2 max_x, Type1 min_y, Type1 max_y) {
        addNewSeries(data, min_x, max_x, min_y, max_y);
        this->update();
    }

    template<typename Type1, typename Type2>
    void plot(QVector<Type1> data, Type2 min_x, Type2 max_x, Type1 min_y, Type1 max_y) {
        _draw_data.clear();
        addNewSeries(data, min_x, max_x, min_y, max_y);
        this->update();
    }

    template<typename Type1, typename Type2>
    void append(Type1 data[], int size_of_data, Type2 min_x, Type2 max_x, Type1 min_y, Type1 max_y) {
        QVector<Type1> vect_data;
        for(int i = 0; i < size_of_data; i++) {
            vect_data.push_back(data[i]);
        }

        addNewSeries(vect_data, min_x, max_x, min_y, max_y);
        this->update();
    }

    template<typename Type1, typename Type2>
    void plot(Type1 data[], int size_of_data, Type2 min_x, Type2 max_x, Type1 min_y, Type1 max_y) {
        _draw_data.clear();
        QVector<Type1> vect_data;
        for(int i = 0; i < size_of_data; i++) {
            vect_data.push_back(data[i]);
        }

        addNewSeries(vect_data, min_x, max_x, min_y, max_y);
        this->update();
    }

    void clearAllSeries();

    void setAxisMultipliers(float stepX, float stepY);

protected:
    virtual void resizeEvent(QResizeEvent *e) override;

private:
    QWidget *_parent;

    uint32_t _frame_left_angle;
    QPair<int, int> _frame_sizes[4];

    struct Hor_Grid_Struct {
        int y_low = 0;
        int y_up = 0;
        QVector<int> x;
    }_hor_grid_lines;

    struct Vert_Grid_Struct {
        int x_left = 0;
        int x_right = 0;
        QVector<int> y;
    }_vert_grid_lines;

    struct Draw_Data {
        QColor line_color = Qt::red;
        double line_width = 2.5;
        int max_y = 0;
        int min_y = 0;
        int max_x = 0;
        int min_x = 0;
        QVector<QVariant> data;
        QVector<QPair<QVariant, int>> data_coord;
    };

    QVector<Draw_Data> _draw_data;

    int _x = 0;
    int _y = 0;
    int _width = 0;
    int _height = 0;
    int _frame_width = 0;
    int _frame_height = 0;
    int grid_text_size = 12;
    float _stepX = 1;
    float _stepY = 1;

    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override;

    void setFrame(int left_tab, int right_tab, int upper_tab, int bottom_tab);
    void setGrid(int hor_grid_lines_count, int vert_grid_lines_count);

    template<typename Type1, typename Type2>
    void addNewSeries(QVector<Type1> data, Type2 min_x, Type2 max_x, Type1 min_y, Type1 max_y) {
        if(data.size() > 0) {
            Draw_Data new_data;
            new_data.max_y = max_y;
            new_data.min_y = min_y;
            new_data.max_x = max_x;
            new_data.min_x = min_x;

            //Делаем отступы для рамки
            int left_tab = QString::number(max_y).size() * grid_text_size + 2 * grid_text_size / 3;
            int right_tab = _width / 20;
            int upper_tab = _height / 20;
            int bottom_tab = grid_text_size + 2 * grid_text_size / 3;

            setFrame(left_tab, right_tab, upper_tab, bottom_tab);
            setGrid(3, 3);

            //Если у нас есть уже записанные серии, то изменим масштаб под новые масштабы для них
            if(_draw_data.size() > 0) {
                for(int j = 0; j < _draw_data.size(); j++) {
                    if((_draw_data[j].max_y != max_y) || (_draw_data[j].min_y != min_y)) {
                        _draw_data[j].data_coord.clear();
                        int size = max_x - 1;
                        for(int i = 0; i < size + 1; i++) {
                            double d_x = double(_frame_width) / double(size - min_x) * double(i - min_x) + _frame_sizes[0].first;
                            auto x = d_x;
                                if((i >= min_x) && (i <= max_x)) {
                                if((_draw_data[j].data[i] <= (max_y + min_y)) && (_draw_data[j].data[i] >= min_y)) {
                                    auto y = _frame_height - ((_draw_data[j].data[i].toInt() - min_y) * (_frame_height - 3)) / max_y + _frame_sizes[0].second - 1.5;
                                    _draw_data[j].data_coord.push_back(QPair(x, y));
                                } else if (_draw_data[j].data[i] > (max_y + min_y)) {
                                    _draw_data[j].data_coord.push_back(QPair(x, _frame_sizes[0].second));
                                } else if (_draw_data[j].data[i] < min_y) {
                                    _draw_data[j].data_coord.push_back(QPair(x, _frame_sizes[3].second));
                                }
                            }
                        }
                    }
                }
            }

            int size = max_x - 1;
            //Если введенный размер получился выше того, что есть, то он будет равен размеру вектора
            if(data.size() < (max_x - min_x)) {
              size = data.size() - 1;
            }
            //Переводим точки в новый диапазон
            for(int i = 0; i < size + 1; i++) {
                double d_x = double(_frame_width) / double(size - min_x) * double(i - min_x) + _frame_sizes[0].first;
                auto x = d_x;
                //Если индекс попадает в диапазон значений по горизонтали
                if((i >= min_x) && (i <= max_x)) {
                    //Если амплидута подходит под заданные границы
                    if((data[i] <= (max_y + min_y)) && (data[i] >= min_y)) {
                        auto y = _frame_height - ((data[i] - min_y) * (_frame_height - 3)) / max_y + _frame_sizes[0].second - 1.5;
                        new_data.data_coord.push_back(QPair(x, y));
                    }
                    //Если амплитуда не попадает в заданные границы, то рисуем линии за рамками
                    else if(data[i] > (max_y + min_y)){
                        new_data.data_coord.push_back(QPair(x, _frame_sizes[0].second));
                    } else if(data[i] < min_y) {
                        new_data.data_coord.push_back(QPair(x, _frame_sizes[3].second));
                    }
                }
                new_data.data.push_back(data[i]);
            }

            //Подбираем цвет для новой серии
            QRandomGenerator random_color;
            random_color.seed(_draw_data.size() + 4);
            uint8_t red = random_color.bounded(40, 255);
            uint8_t green = random_color.bounded(40, 255);
            uint8_t blue = random_color.bounded(40, 255);
            //Такие условия нужны, чтобы отрисовывался всегда яркий цвет
            if(red > 200) {
                green = random_color.bounded(20, 80);
                blue = random_color.bounded(20, 80);
            }
            if(green > 200) {
                red = random_color.bounded(20, 80);
                blue = random_color.bounded(20, 80);
            }
            if(blue > 200) {
                red = random_color.bounded(20, 80);
                green = random_color.bounded(20, 80);
            }
            new_data.line_color = QColor::fromRgb(red, green, blue);
            _draw_data.push_back(new_data);
        }
    }
};

#endif // OGLPLOT_H
