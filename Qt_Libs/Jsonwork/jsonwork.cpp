#include "jsonwork.h"

JsonWork::JsonWork()
{

}

QString JsonWork::SaveConfig(QVector<QJsonValue> data) {
    if(data.length() < GBO_SET_CONFIG_params_names.length() - 1) {
        qDebug("'data' length less than SET_CONFIG!");
        return "";
    } else {
        QString format = "hh-mm  dd_MM_yyyy";
        QString format2 = "hh:mm:ss dd.MM.yyyy";
        QString datetime = QDateTime::currentDateTime().toString(format);

        unique_ptr<QFile> fileJson = make_unique<QFile>(QDir::currentPath()+ "/CONFIGS/" + datetime + ".json");
        fileJson->open(QIODevice::WriteOnly);

        QJsonObject params;
        uint32_t a = 0;
        for(QString i : GBO_SET_CONFIG_params_names) {
            if(i == "Datetime") {
               params.insert(i, QDateTime::currentDateTime().toString(format2));
            } else {
                params.insert(i, data[a]);
            }

            a++;
        }

        fileJson->write(QJsonDocument(QJsonObject(params)).toJson());
        fileJson->close();

        return datetime;
    }
}

QString JsonWork::SaveConfig(C_SVSP::Config_Struct data) {
    QString format = "hh-mm  dd_MM_yyyy";
    QString format2 = "hh:mm:ss dd.MM.yyyy";
    QString datetime = QDateTime::currentDateTime().toString(format);

    unique_ptr<QFile> fileJson = make_unique<QFile>(QDir::currentPath()+ "/CONFIGS/" + datetime + ".json");

    if (fileJson->open(QIODevice::WriteOnly)) {
        QJsonObject params;

        params.insert(GBO_SET_CONFIG_params_names[0], data.Cfg_Word);
        params.insert(GBO_SET_CONFIG_params_names[1], data.Ch1_Frequency);
        params.insert(GBO_SET_CONFIG_params_names[2], data.Ch2_Frequency);
        params.insert(GBO_SET_CONFIG_params_names[3], data.Ch1_Distance);
        params.insert(GBO_SET_CONFIG_params_names[4], data.Ch2_Distance);
        params.insert(GBO_SET_CONFIG_params_names[5], data.Ch1_Duration);
        params.insert(GBO_SET_CONFIG_params_names[6], data.Ch2_Duration);
        params.insert(GBO_SET_CONFIG_params_names[7], data.Sound_Speed);
        params.insert(GBO_SET_CONFIG_params_names[8], data.Ch1_G1);
        params.insert(GBO_SET_CONFIG_params_names[9], data.Ch1_G2);
        params.insert(GBO_SET_CONFIG_params_names[10], data.Ch1_G3);
        params.insert(GBO_SET_CONFIG_params_names[11], data.Ch2_G1);
        params.insert(GBO_SET_CONFIG_params_names[12], data.Ch2_G2);
        params.insert(GBO_SET_CONFIG_params_names[13], data.Ch2_G3);
        params.insert(GBO_SET_CONFIG_params_names[14], data.Ch1_Count);
        params.insert(GBO_SET_CONFIG_params_names[15], data.Ch2_Count);

        QJsonArray array_ch1;
        for(uint32_t i = 0; i < data.Ch1_Count; i++) {
            array_ch1.push_back((float)data.Ch1_Data[i]);
        }
        params.insert(GBO_SET_CONFIG_params_names[16], array_ch1);

        QJsonArray array_ch2;
        for(uint32_t i = 0; i < data.Ch2_Count; i++) {
            array_ch2.push_back((float)data.Ch2_Data[i]);
        }
        params.insert(GBO_SET_CONFIG_params_names[17], array_ch2);

        params.insert(GBO_SET_CONFIG_params_names[18], QDateTime::currentDateTime().toString(format2));

        fileJson->write(QJsonDocument(QJsonObject(params)).toJson());
        fileJson->close();

        return datetime;
    } else {
        return 0;
    }
}

QString JsonWork::SaveConfig(C_SVSP::Config_Struct data, QString save_filename) {
    QString format = "hh-mm  dd_MM_yyyy";
    QString format2 = "hh:mm:ss dd.MM.yyyy";
    QString datetime = QDateTime::currentDateTime().toString(format);

    unique_ptr<QFile> fileJson = make_unique<QFile>(QDir::currentPath()+ "/CONFIGS/" + save_filename + ".json");

    if (fileJson->open(QIODevice::WriteOnly)) {
        QJsonObject params;

        params.insert(GBO_SET_CONFIG_params_names[0], data.Cfg_Word);
        params.insert(GBO_SET_CONFIG_params_names[1], data.Ch1_Frequency);
        params.insert(GBO_SET_CONFIG_params_names[2], data.Ch2_Frequency);
        params.insert(GBO_SET_CONFIG_params_names[3], data.Ch1_Distance);
        params.insert(GBO_SET_CONFIG_params_names[4], data.Ch2_Distance);
        params.insert(GBO_SET_CONFIG_params_names[5], data.Ch1_Duration);
        params.insert(GBO_SET_CONFIG_params_names[6], data.Ch2_Duration);
        params.insert(GBO_SET_CONFIG_params_names[7], data.Sound_Speed);
        params.insert(GBO_SET_CONFIG_params_names[8], data.Ch1_G1);
        params.insert(GBO_SET_CONFIG_params_names[9], data.Ch1_G2);
        params.insert(GBO_SET_CONFIG_params_names[10], data.Ch1_G3);
        params.insert(GBO_SET_CONFIG_params_names[11], data.Ch2_G1);
        params.insert(GBO_SET_CONFIG_params_names[12], data.Ch2_G2);
        params.insert(GBO_SET_CONFIG_params_names[13], data.Ch2_G3);
        params.insert(GBO_SET_CONFIG_params_names[14], data.Ch1_Count);
        params.insert(GBO_SET_CONFIG_params_names[15], data.Ch2_Count);

        QJsonArray array_ch1;
        for(uint32_t i = 0; i < data.Ch1_Count; i++) {
            array_ch1.push_back((float)data.Ch1_Data[i]);
        }
        params.insert(GBO_SET_CONFIG_params_names[16], array_ch1);

        QJsonArray array_ch2;
        for(uint32_t i = 0; i < data.Ch1_Count; i++) {
            array_ch2.push_back((float)data.Ch2_Data[i]);
        }
        params.insert(GBO_SET_CONFIG_params_names[17], array_ch2);

        params.insert(GBO_SET_CONFIG_params_names[18], QDateTime::currentDateTime().toString(format2));

        fileJson->write(QJsonDocument(QJsonObject(params)).toJson());
        fileJson->close();

        return datetime;
    } else {
        return 0;
    }
}

/*QVector<QJsonValue> JsonWork::LoadConfig(QString filename) {
    unique_ptr<QFile> fileJson = make_unique<QFile>(QDir::currentPath()+ "/CONFIGS/" + filename + ".json");
    fileJson->open(QIODevice::ReadOnly);
    QString text_data = fileJson->readAll();

    QJsonDocument Json_doc = QJsonDocument::fromJson(text_data.toUtf8());
    QJsonObject Json_obj = Json_doc.object();
    QVector<QJsonValue> data;

    for(auto i : Json_obj) {
        data.push_back(i);
    }

    return data;
}*/

/*QJsonObject JsonWork::LoadConfig(QString path) {
    unique_ptr<QFile> fileJson = make_unique<QFile>(path);
    if(fileJson->open(QIODevice::ReadOnly)) {
        QString text_data = fileJson->readAll();

        QJsonDocument Json_doc = QJsonDocument::fromJson(text_data.toUtf8());
        QJsonObject Json_obj = Json_doc.object();

        return Json_obj;

    } else {
        return {};
    }
}*/

C_SVSP::Config_Struct JsonWork::LoadConfig(QString path) {
    unique_ptr<QFile> fileJson = make_unique<QFile>(path);
    if(fileJson->open(QIODevice::ReadOnly)) {
        QString text_data = fileJson->readAll();

        QJsonDocument Json_doc = QJsonDocument::fromJson(text_data.toUtf8());
        QJsonObject Json_obj = Json_doc.object();

        C_SVSP::Config_Struct data;

        data.Cfg_Word = Json_obj[GBO_SET_CONFIG_params_names[0]].toInt();
        data.Ch1_Frequency = Json_obj[GBO_SET_CONFIG_params_names[1]].toInt();
        data.Ch2_Frequency = Json_obj[GBO_SET_CONFIG_params_names[2]].toInt();
        data.Ch1_Distance = Json_obj[GBO_SET_CONFIG_params_names[3]].toInt();
        data.Ch2_Distance = Json_obj[GBO_SET_CONFIG_params_names[4]].toInt();
        data.Ch1_Duration = Json_obj[GBO_SET_CONFIG_params_names[5]].toInt();
        data.Ch2_Duration = Json_obj[GBO_SET_CONFIG_params_names[6]].toInt();
        data.Sound_Speed = Json_obj[GBO_SET_CONFIG_params_names[7]].toDouble();
        data.Ch1_G1 = Json_obj[GBO_SET_CONFIG_params_names[8]].toDouble();
        data.Ch1_G2 = Json_obj[GBO_SET_CONFIG_params_names[9]].toDouble();
        data.Ch1_G3 = Json_obj[GBO_SET_CONFIG_params_names[10]].toDouble();
        data.Ch2_G1 = Json_obj[GBO_SET_CONFIG_params_names[11]].toDouble();
        data.Ch2_G2 = Json_obj[GBO_SET_CONFIG_params_names[12]].toDouble();
        data.Ch2_G3 = Json_obj[GBO_SET_CONFIG_params_names[13]].toDouble();
        data.Ch1_Count = Json_obj[GBO_SET_CONFIG_params_names[14]].toInt();
        data.Ch2_Count = Json_obj[GBO_SET_CONFIG_params_names[15]].toInt();

        data.Ch1_Data = (float *)malloc(data.Ch1_Count * C_SVSP_CONFIG_STRUCT_CH_DATA_SIZE);
        data.Ch2_Data = (float *)malloc(data.Ch2_Count * C_SVSP_CONFIG_STRUCT_CH_DATA_SIZE);

        {
            QJsonArray array_ch1 = Json_obj[GBO_SET_CONFIG_params_names[16]].toArray();
            for(uint32_t i = 0; i < data.Ch1_Count; i++) {
                data.Ch1_Data[i] = array_ch1[i].toDouble();
            }
        }

        {
            QJsonArray array_ch2 = Json_obj[GBO_SET_CONFIG_params_names[17]].toArray();
            for(uint32_t j = 0; j < data.Ch2_Count; j++) {
                data.Ch2_Data[j] = array_ch2[j].toDouble();
            }
        }

        return data;
    } else {
        return {};
    }
}
