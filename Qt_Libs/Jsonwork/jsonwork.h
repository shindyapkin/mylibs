#ifndef JSONWORK_H
#define JSONWORK_H

#include <QVector>
#include <QString>
#include <iostream>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QDateTime>
#include <QDir>
#include <QFile>
#include <memory>

#include <C_SVSP.h>

using namespace std;

const QVector<QString> GBO_SET_CONFIG_params_names = {"Configuration word GBO",         //1
                                                     "Working Frequency GA1, kHz",      //2
                                                     "Working Frequency GA2, kHz",      //3
                                                     "Distance GA1, m",                 //4
                                                     "Distance GA2, m",                 //5
                                                     "Pulse time GA1, mcs",             //6
                                                     "Pulse time GA2, mcs",             //7
                                                     "Speed of sound, m/s",             //8
                                                     "Param G1 for GA1",                //9
                                                     "Param G2 for GA1",                //10
                                                     "Param G3 for GA1",                //11
                                                     "Param G1 for GA2",                //12
                                                     "Param G2 for GA2",                //13
                                                     "Param G3 for GA2",                //14
                                                     "Number of VARU zones GA1 NZ1",    //15
                                                     "Number of VARU zones GA2 NZ2",    //16
                                                     "Array of gains for GA1",          //17
                                                     "Array of gains for GA2",          //18
                                                     "Datetime"};                       //19

/*struct SET_CONFIG {
   uint8_t signal_from_PC = 0x01;
   uint16_t datogram_length = 0x03;
   uint8_t configuration_word = 0;
   uint16_t work_freq_ga1 = 0;
   uint16_t work_freq_ga2 = 0;
   QVector<float> gains_for_ga1;
   QVector<float> gains_for_ga2;
   uint8_t CRC_NMEA = 0;
};*/

class JsonWork
{
public:
    JsonWork();

    QString SaveConfig(QVector<QJsonValue> data);
    QString SaveConfig(C_SVSP::Config_Struct data);
    QString SaveConfig(C_SVSP::Config_Struct data, QString save_filename);
//    QVector<QJsonValue> LoadConfig(QString filename);
//    QJsonObject LoadConfig(QString path);
    C_SVSP::Config_Struct LoadConfig(QString path);

private:

};

#endif // JSONWORK_H
