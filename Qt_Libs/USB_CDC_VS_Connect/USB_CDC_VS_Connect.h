/*!
  @file USB_CDC_VS_Connect.h
  @brief Библиотека для работы по протоколу USB Comminication Device Class (Virtual COM Port)
*/

#ifndef USB_CDC_VS_CONNECT_H
#define USB_CDC_VS_CONNECT_H

#include <QString>
#include <QElapsedTimer>
#include <windows.h>

/*!
 @class USB_CDC_VS_Connect
 @brief Класс, позволяющий взаимодействовать с USB CDC
*/
class USB_CDC_VS_Connect
{
public:
    ~USB_CDC_VS_Connect() = default;

    /*!
      @fn bool ConfigurateUSB_CDC_VS(QString PortName)
      @brief Функция создания порта
      @param PortName: имя порта, к которому будет происходить подключение
      @retval При успешном подключении возвращается 1, иначе - 0
    */
    bool ConfigurateUSB_CDC_VS(QString PortName);

    /*!
      @fn void DisconnectUSB_CDC_VS()
      @brief Функция отключения от порта
      @retval При успешном отключении возвращается 0, иначе - 1
    */
    bool DisconnectUSB_CDC_VS();

    /*!
      @fn uint32_t TransmitData_VS(char *TxData, uint32_t size)
      @brief Функция отправляет сообщение на порт
      @param *TxData: - указатель на байтовый массив, который будет отправлен
      @param size: - размер массива, который будет отправлен (байт)
      @details Нельзя забывать, что отправляется БАЙТОВЫЙ массив, поэтому размер отправки указываем в байтах
      @retval Возвращает количество успешно отправленных байт
    */
    uint32_t TransmitData_VS(char *TxData, uint32_t size);

    /*!
      @fn uint32_t DataReceive(char *RxData, uint32_t size, uint16_t timeout)
      @brief Функция принимает сообщение из порта
      @param *RxData: - указатель на байтовый массив, на который будет вести прием
      @param size: - размер принимаемого массива (байт)
      @param timeout: - время, которое команда будет ожидать прием (мс)
      @details Нельзя забывать, что принимается БАЙТОВЫЙ массив, поэтому размер приема указываем в байтах
      @retval Возвращает количество принятых байт, если же время ожидания данных превысит timeout - 0
    */
    uint32_t DataReceive(char *RxData, uint32_t size, uint16_t timeout);

    /*!
      @fn uint8_t TransmitDataWithConfirmation(char *TxData, uint32_t size, uint32_t timeout)
      @brief Функция отправки данных с последующим подтверждением приема
      @param *TxData: - указатель на байтовый массив, который будет отправлен
      @param size: - размер массива, который будет отправлен (байт)
      @param timeout: - время ожидания ответного сообщения (мс)
      @details По своей сути является объединением функций отправки и приема,
      при котором ожидается ответное сообщение подтверждения успешной отправки
      @retval Если устройство отправило сообщение подтверждения (размер - 1 байт), то возвращает его.
      Если же время ожидания ответного сообщения превысило timeout, то возвращается 0
    */
    uint8_t TransmitDataWithConfirmation(char *TxData, uint32_t size, uint32_t timeout);

private:
    /*!
      @var serialHandle
      @brief Серийный порт
    */
    HANDLE serialHandle;

    /*!
      @var dwBytesWritten
      @brief Переменная для количества отправленных байт
    */
    DWORD dwBytesWritten;

    /*!
      @var dwBytesRead
      @brief Переменная для количества принятых байт
    */
    DWORD  dwBytesRead;

    /*!
      @var Current_connection
      @brief Держит в себе состояние текущего подключения
    */
    bool Current_connection = 0;
};

#endif // USB_CDC_VS_CONNECT_H
