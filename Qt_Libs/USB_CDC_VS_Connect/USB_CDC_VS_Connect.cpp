#include "USB_CDC_VS_Connect.h"

//Создаем порт
bool USB_CDC_VS_Connect::ConfigurateUSB_CDC_VS(QString PortName) {
    if(!Current_connection) {
        PortName.prepend(L"\\\\.\\");
        serialHandle = CreateFile((LPCWSTR)PortName.data(), GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

        //Задаем параметры соединения COM порта
        DCB serialParams = {0};
        serialParams.DCBlength = sizeof(serialParams);

        //Задаем новое занчение состоянию подключения
        Current_connection = GetCommState(serialHandle, &serialParams);
        serialParams.BaudRate = 115200;
        serialParams.ByteSize = DATABITS_8;
        serialParams.StopBits = ONESTOPBIT;
        serialParams.Parity = (char)PARITY_NONE;
        SetCommState(serialHandle, &serialParams);

        //Задаем таймауты на прием и передачу
        COMMTIMEOUTS timeout;
        timeout.ReadIntervalTimeout = 1;
        timeout.ReadTotalTimeoutConstant = 1;
        timeout.ReadTotalTimeoutMultiplier = 1;
        timeout.WriteTotalTimeoutConstant = 0;
        timeout.WriteTotalTimeoutMultiplier = 0;

        SetCommTimeouts(serialHandle, &timeout);

        //Доп. проверка на подключение. Если оно удачно, то возвращаем 1
        if(Current_connection) {
           return 1;
        } else {
            //Если подключение неудачно, в любом из случаев возвращаем 0
            return 0;
        }
    } else {
        return 0;
    }
}

//Отключаемся от порта
bool USB_CDC_VS_Connect::DisconnectUSB_CDC_VS() {
    //Закрываем handle и ставим значение состояния подклюения в 0
    Current_connection = !CloseHandle(serialHandle);
    return Current_connection;
}

//Функция отправки сообщения
uint32_t USB_CDC_VS_Connect::TransmitData_VS(char *TxData, uint32_t size) {
    //Отправляем данные
    WriteFile(serialHandle, (char*)TxData, size, &dwBytesWritten, NULL);
    //Возвращаем количество записанных байт
    return dwBytesWritten;
}

//Функция приема сообщения
uint32_t USB_CDC_VS_Connect::DataReceive(char *RxData, uint32_t size, uint16_t timeout) {
    uint32_t Received = 0;
    QElapsedTimer timer;
    timer.start();
    //Пока приняли байт меньше чем нужно, сидим в цикле
    while((Received < size)) {
        //Пытаемся считать 1 байт
        ReadFile(serialHandle, (char*)RxData + Received, 1, &dwBytesRead, NULL);
        //Суммируем количество принятых байт
        Received += dwBytesRead;
        //Если данные не приходят и время истекло, то возвращаем 0
        if((dwBytesRead == 0) && (timer.elapsed() >= timeout)) {
            return 0;
        }
    }
    return Received;
}

//Функция отправки данных с последующим подтверждением приема
uint8_t USB_CDC_VS_Connect::TransmitDataWithConfirmation(char *TxData, uint32_t size, uint32_t timeout) {
    uint8_t repeat_times = 3;
    uint8_t current_time = 0;

    uint8_t conf_2 = 0;

    while((current_time < repeat_times) && (conf_2 == 0)) {

        //Отправляем данные
        WriteFile(serialHandle, (char*)TxData, size, &dwBytesWritten, NULL);

        uint32_t Received = 0;

        QElapsedTimer timer;
        timer.start();

        //Пока приняли байт меньше чем нужно, сидим в цикле
        while((Received < 1)) {
            //Пытаемся считать 1 байт
            ReadFile(serialHandle, (char*)&conf_2, 1, &dwBytesRead, NULL);
            Received += dwBytesRead;

            if((dwBytesRead == 0) || (timer.elapsed() >= timeout)) {
                current_time++;
                break;
            }
        } 
    }
    return conf_2;
}
