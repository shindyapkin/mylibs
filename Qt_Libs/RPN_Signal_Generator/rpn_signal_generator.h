#ifndef RPN_SIGNAL_GENERATOR_H
#define RPN_SIGNAL_GENERATOR_H

#include "QString"
#include "QVector"
#include <QFuture>
#include <QtConcurrent>

#include "math.h"
#include <thread>

#define type double

class RPN_Signal_Generator
{
public:
    RPN_Signal_Generator();
    void getRawFormula(QString formula);
//    double calculateFormula(double Tb, double Td, double U0, double f, double n, double i);

    /*template<typename Type1>
    void calculateSignal(Type1 *signal, double Tb, double Td, double U0, double f, double n, double time_step, uint32_t size) {
        double tb = Tb;
        double td = Td;
        for(uint32_t i = 0; i < size; i++) {
            signal[i] = calculateFormula(tb, td, U0, f, n, i * time_step);
        }
        int t = 0;
    }*/

    uint16_t calculateFormula(type Tb, type Td, type U0, type f, uint16_t n, double i);

    template<typename Type1>
        void calculateSignal(Type1 *signal, uint32_t max_val, uint32_t Tb, uint32_t Td, uint16_t U0, uint32_t f, uint16_t n, uint8_t SNR, double time_step) {
            double tb = Tb * time_step;
            double td = Td * time_step;

            srand(Tb);
            uint32_t average = max_val / 2;
            uint32_t noisborder = 1;
            uint32_t noiseborder_div2 = 0;
            if(SNR > 0) {
                noisborder = U0 * (SNR / 100.0f);
                noiseborder_div2 = noisborder / 2;
            }

            for(uint32_t j = 0; j < Tb; j++) {
                signal[j] = average + rand() % noisborder - noiseborder_div2;
            }

            for(uint32_t i = Tb; i < Td; i++) {
                signal[i] = calculateFormula(tb, td, U0, f, n, (i * time_step)) + rand() % noisborder - noiseborder_div2;
            }

//            uint32_t start_index = 0;
//            uint32_t half_data_size = Td / 2;

            /*auto future1 = QtConcurrent::run(&_thread_pool,
                              [this, signal, max_val, Tb, Td, U0, f, n, SNR, start_index, half_data_size, time_step] {
                                    calculateInThread<Type1>(signal, max_val, Tb, Td, U0, f, n, SNR, start_index, half_data_size - 1, time_step);
                                 }
                              );

            auto future2 = QtConcurrent::run(&_thread_pool,
                              [this, signal, max_val, Tb, Td, U0, f, n, SNR, half_data_size, time_step] {
                                    calculateInThread<Type1>(signal, max_val, Tb, Td, U0, f, n, SNR, half_data_size, Td, time_step);
                                 }
                              );

            while (future1.isRunning() || future2.isRunning()) {

            }*/

            /*uint32_t half_data_size = Td / 2;
            std::thread thread1(&calculateInThread<Type1>, this, signal, max_val, Tb, Tb, U0, f, n, SNR, 0, time_step);
            std::thread thread2(&calculateInThread<Type1>, this, signal, max_val, Tb, Td, U0, f, n, SNR, half_data_size, time_step);
            thread1.join();
            thread2.join();*/

    }


private:
    QVector<QString> _formula;

    QThreadPool _thread_pool;

//private slots:
    template<typename Type1>
    void calculateInThread(Type1 *signal, uint32_t max_val, uint32_t Tb, uint32_t Td, uint16_t U0, uint32_t f, uint16_t n, uint8_t SNR, uint32_t start_index, uint32_t stop_index, double time_step) {
        double tb = Tb * time_step;
        double td = Td * time_step;


        qDebug() << std::this_thread::get_id;

        srand(Tb);
        uint32_t average = max_val / 2;
        uint32_t noisborder = 1;
        uint32_t noiseborder_div2 = 0;
        if(SNR > 0) {
            noisborder = U0 * (SNR / 100.0f);
            noiseborder_div2 = noisborder / 2;
        }

        for(uint32_t i = start_index; i < stop_index; i++) {
            if(i < Tb) {
                signal[i] = average + rand() % noisborder - noiseborder_div2;
            } else {
                signal[i] = calculateFormula(tb, td, U0, f, n, (i * time_step)) + rand() % noisborder - noiseborder_div2;
            }
        }
    }
};

//void calculateFormula(QString formula);

#endif // RPN_SIGNAL_GENERATOR_H
