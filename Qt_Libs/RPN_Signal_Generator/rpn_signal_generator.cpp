#include "rpn_signal_generator.h"

RPN_Signal_Generator::RPN_Signal_Generator()
{
    _thread_pool.setMaxThreadCount(2);
}

void RPN_Signal_Generator::getRawFormula(QString formula)
{
//    QVector<QString> operators;
    _formula.clear();
    for(int i = 0; i < formula.size(); i++) {
        QString value;
        int j = 0;
        while(formula[i] != " " && i < formula.size()) {
            value[j] = formula[i];
            i++;
            j++;
        }
        if(value.size() > 0) {
            _formula.push_back(value);
        }
    }
}

uint16_t RPN_Signal_Generator::calculateFormula(type Tb, type Td, type U0, type f, uint16_t n, double i)
{
    QVector<double> stack;

    for(int t = 0; t < _formula.size(); t++) {
        QString current_value = _formula.at(t);
        int stack_size = stack.size() - 1;

        if(current_value == "+") {
            stack[stack_size] = stack[stack_size - 1] + stack[stack_size];
            stack.remove(stack_size - 1);
        }
        else if(current_value == "-") {
            stack[stack_size] = stack[stack_size - 1] - stack[stack_size];
            stack.remove(stack_size - 1);
        }
        else if(current_value == "*") {
            stack[stack_size] = stack[stack_size - 1] * stack[stack_size];
            stack.remove(stack_size - 1);
        }
        else if(current_value == "/") {
            stack[stack_size] = stack[stack_size - 1] / stack[stack_size];
            stack.remove(stack_size - 1);
        }
        else if(current_value == "sin") {
            stack[stack_size] = sin(stack[stack_size]);
        }
        else if(current_value == "^") {
            stack[stack_size] = pow(stack[stack_size - 1], stack[stack_size]);
            stack.remove(stack_size - 1);
        }
        else if(current_value == "sqrt") {
            stack[stack_size] = sqrt(stack[stack_size]);
        }
        else if(current_value == "exp") {
           stack[stack_size] = exp(stack[stack_size]);
        }
        else if(std::atoi(current_value.toUtf8()) != 0) {
            stack.push_back(current_value.toDouble());
        }
        else if(current_value == "Tb" || current_value == "tb") {
            stack.push_back(Tb);
        }
        else if(current_value == "Td" || current_value == "td") {
            stack.push_back(Td);
        }
        else if(current_value == "U0" || current_value == "u0") {
            stack.push_back(U0);
        }
        else if(current_value == "F" || current_value == "f") {
            stack.push_back(f);
        }
        else if(current_value == "n") {
            stack.push_back(n);
        }
        else if(current_value == "pi") {
            stack.push_back(M_PI);
        }
        else if(current_value == "i") {
            stack.push_back(i);
        }
    }
    return stack.last();
}

/*void calculateFormula(QString formula) {
    QVector<QString> operators;
    for(int i = 0; i < formula.size(); i++) {
        QString value;
        int j = 0;
        while(formula[i] != " " && i < formula.size()) {
            value[j] = formula[i];
            i++;
            j++;
        }
        if(value.size() > 0) {
            operators.push_back(value);
        }
    }

    double x = 5;
    double result = 0;
    QVector<double> stack;

    for(int t = 0; t < operators.size(); t++) {
        QString current_value = operators.at(t);
        int stack_size = stack.size() - 1;

        if(current_value == "+") {
            stack[stack_size] = stack[stack_size - 1] + stack[stack_size];
            stack.remove(stack_size - 1);
        }
        else if(current_value == "-") {
            stack[stack_size] = stack[stack_size - 1] - stack[stack_size];
            stack.remove(stack_size - 1);
        }
        else if(current_value == "*") {
            stack[stack_size] = stack[stack_size - 1] * stack[stack_size];
            stack.remove(stack_size - 1);
        }
        else if(current_value == "/") {
            stack[stack_size] = stack[stack_size - 1] / stack[stack_size];
            stack.remove(stack_size - 1);
        }
        else if(current_value == "sin") {
            stack[stack_size] = sin(stack[stack_size]);
        }
        else if(current_value == "^") {
            stack[stack_size] = pow(stack[stack_size - 1], stack[stack_size]);
            stack.remove(stack_size - 1);
        }
        else if(current_value == "sqrt") {
            stack[stack_size] = sqrt(stack[stack_size]);
        }
        else if(current_value == "exp") {
           stack[stack_size] = exp(stack[stack_size]);
        }
        else if(std::atoi(current_value.toUtf8())) {
            stack.push_back(current_value.toDouble());
        }
        else if(current_value == "x") {
            stack.push_back(x);
        }

    }
    result = stack.last();
    int yt = 0;
}*/
