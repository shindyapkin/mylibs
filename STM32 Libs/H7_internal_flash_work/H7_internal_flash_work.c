/*
 * internal_flash_work.c
 *
 *  Created on: Feb 10, 2021
 *      Author: shindyapkin
 */

#include "H7_internal_flash_work.h"

//Функция разблокировки функции записи в память
void flash_unlock(void) {
	FLASH->KEYR1 = FLASH_KEY1;
	FLASH->KEYR2 = FLASH_KEY1;

	FLASH->KEYR1 = FLASH_KEY2;
	FLASH->KEYR2 = FLASH_KEY2;
}

//Функция блокировки функции записи в память
void flash_lock() {
	FLASH->CR1 |= FLASH_CR_LOCK;
	FLASH->CR2 |= FLASH_CR_LOCK;
}

//Проверка flash памяти на готовность к записи
uint8_t flash_ready(void) {
	return !(FLASH->SR1 & FLASH->SR2 & FLASH_SR_BSY);
}

//Функция очистки всей flash памяти
void flash_erase_all_pages(void) {
	//Устанавливаем бит стирания ВСЕХ страниц
	FLASH->CR1 |= FLASH_CR_BER;
	FLASH->CR2 |= FLASH_CR_BER;

	//Начать стирание
	FLASH->CR1 |= FLASH_CR_START;
	FLASH->CR2 |= FLASH_CR_START;

	// Ожидание готовности
	while(!flash_ready()) {}
	FLASH->CR1 &= FLASH_CR_BER;
	FLASH->CR2 &= FLASH_CR_BER;
}

///Функция очистки страницы
void flash_erase_page(uint32_t address) {
	uint32_t bank = FLASH_BANK_1;
	if(address >= 0x08100000) {
		bank = FLASH_BANK_2;
	}

	if(bank == FLASH_BANK_1) {
		//Устанавливаем бит стирания одной страницы
		FLASH->CR1 = FLASH_CR_SER;
		//Задаем её адрес
		FLASH->CRCSADD1 = address;
		//Запускаем стирание
		FLASH->CR1 |= FLASH_CR_START;
		//Ждем пока страница сотрется
		while(!flash_ready());
		//Сбрасываем бит обратно
		FLASH->CR1 &= ~FLASH_CR_SER;
	} else {
		//Устанавливаем бит стирания одной страницы
		FLASH->CR2 = FLASH_CR_SER;
		//Задаем её адрес
		FLASH->CRCSADD2 = address;
		//Запускаем стирание
		FLASH->CR2 |= FLASH_CR_START;
		//Ждем пока страница сотрется
		while(!flash_ready());
		//Сбрасываем бит обратно
		FLASH->CR2 &= ~FLASH_CR_SER;
	}




}

//Функция записи данных во flash
void flash_write(uint32_t address,uint32_t data) {
	__IO uint32_t *dest_addr = (__IO uint32_t *)address;
	__IO uint32_t *src_addr = (__IO uint32_t*)data;
	uint8_t row_index = FLASH_NB_32BITWORD_IN_FLASHWORD;

	uint32_t bank = FLASH_BANK_1;
	if(address >= 0x08100000) {
		bank = FLASH_BANK_2;
	}

	if(bank == FLASH_BANK_1) {
		FLASH->CR1 = FLASH_CR_PG;
	} else {
		FLASH->CR2 = FLASH_CR_PG;
	}

	do
	{
	  *dest_addr = *src_addr;
	  dest_addr++;
	  src_addr++;
	  row_index--;
	} while (row_index != 0U);

	if(bank == FLASH_BANK_1) {
		FLASH->CR1 &= ~FLASH_CR_PG;
	} else {
		FLASH->CR2 &= ~FLASH_CR_PG;
	}
}

//Функция чтения flash памяти
uint32_t flash_read(uint32_t address)
{
    return (*(__IO uint32_t*)address);
}

