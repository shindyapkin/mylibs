/*
 * internal_flash_work.h
 *
 *  Created on: Feb 10, 2021
 *      Author: shindyapkin
 */

#include "main.h"

#ifndef INC_INTERNAL_FLASH_WORK_H_
#define INC_INTERNAL_FLASH_WORK_H_

//Функция разблокировки функции записи в память
void flash_unlock(void);

//Функция блокировки функции записи в память
void flash_lock();

//Проверка flash памяти на готовность к записи
uint8_t flash_ready(void);

//Функция очистки всей flash памяти
void flash_erase_all_pages(void);

///Функция очистки страницы
void flash_erase_page(uint32_t address);

//Функция записи данных во flash
void flash_write(uint32_t address,uint32_t data);

//Функция чтения flash памяти
uint32_t flash_read(uint32_t address);

#endif /* INC_INTERNAL_FLASH_WORK_H_ */
