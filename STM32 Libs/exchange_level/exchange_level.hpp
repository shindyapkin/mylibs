/*
 * exchange_level.hpp
 *
 *  Created on: Apr 30, 2021
 *      Author: shindyapkin
 */

#ifndef INC_EXCHANGE_LEVEL_HPP_
#define INC_EXCHANGE_LEVEL_HPP_

#include "main.h"

//Дефайны для использования периферии
#define USE_USB_CDC
//#define USE_UART

//Если используем в проекте USB
#ifdef USE_USB_CDC

	extern "C" {
		#include "usbd_cdc_if.h"
		#include "USB_Connect.h"
	}

	class EXCHANGE_USB {
	public:
		HAL_StatusTypeDef TransmitData(uint8_t *out_message, uint32_t size);
		HAL_StatusTypeDef RecieveData(uint8_t *income_message, uint32_t size, uint16_t Timeout);
		uint32_t RecieveAllData(uint8_t *income_message, uint16_t Timeout);
	};

#endif

//Если используем в проекте UART
#ifdef USE_UART
	class EXCHANGE_UART {
	public:
		EXCHANGE_UART() = delete;

		EXCHANGE_UART(UART_HandleTypeDef *huart);

		HAL_StatusTypeDef TransmitData(uint8_t *pData, uint16_t Size, uint32_t Timeout);
		HAL_StatusTypeDef ReceiveData(uint8_t *pData, uint16_t Size, uint32_t Timeout);
		uint32_t RecieveAllData(uint8_t *pData, uint32_t Timeout);

	private:
		UART_HandleTypeDef *_huart;

	};
#endif


#endif /* INC_EXCHANGE_LEVEL_HPP_ */
