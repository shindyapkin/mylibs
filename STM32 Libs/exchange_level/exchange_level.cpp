/*
 * exchange_level.cpp
 *
 *  Created on: Apr 30, 2021
 *      Author: shindyapkin
 */


#include "exchange_level.hpp"

//Если используем в проекте USB
#ifdef USE_USB_CDC
	HAL_StatusTypeDef EXCHANGE_USB::TransmitData(uint8_t *out_message, uint32_t size) {
		return (HAL_StatusTypeDef)Transmit_Message(out_message, size);
	}

	//Функция приема (почти то же самое, что и по прерыванию, но работает только по вызову и с заносом в массив)
	HAL_StatusTypeDef EXCHANGE_USB::RecieveData(uint8_t *income_message, uint32_t size, uint16_t Timeout) {
		return (HAL_StatusTypeDef)Recieve_Message(income_message, size, Timeout);
	}

	uint32_t EXCHANGE_USB::RecieveAllData(uint8_t *income_message, uint16_t Timeout) {
		return RecieveAll(income_message, Timeout);
	}
#endif

#ifdef USE_UART
	EXCHANGE_UART::EXCHANGE_UART(UART_HandleTypeDef *huart)
	: _huart(huart)
	{

	}

	HAL_StatusTypeDef EXCHANGE_UART::TransmitData(uint8_t *pData, uint16_t Size, uint32_t Timeout) {
		return HAL_UART_Transmit(_huart, pData, Size, Timeout);
	}

	HAL_StatusTypeDef EXCHANGE_UART::ReceiveData(uint8_t *pData, uint16_t Size, uint32_t Timeout) {
		return HAL_UART_Receive(_huart, pData, Size, Timeout);
	}

	uint32_t EXCHANGE_UART::RecieveAll(uint8_t *pData, uint32_t Timeout) {
		uint32_t inc_data = 0;
		uint32_t inc_timout = 0;

		while((ReceiveData(pData, 1, 1)) != HAL_OK) {
			inc_timout++;

			if(inc_timout > Timeout) {
				return 0;
			}
		}

		inc_data++;
		while((ReceiveData(pData + inc_data, 1, 1)) == HAL_OK) {
			inc_data++;
		}

		return inc_data;
	}
#endif
