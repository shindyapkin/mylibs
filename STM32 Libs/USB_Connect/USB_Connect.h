/*
 * USB_Connect.h
 *
 *  Created on: Oct 13, 2020
 *      Author: senya
 */
#include "main.h"
#include "usbd_cdc_if.h"
#include "DAC_AD9744_Work.h"

extern USBD_HandleTypeDef hUsbDeviceFS;
//uint8_t input_buffer[5000];

#ifndef INC_USB_CONNECT_H_
#define INC_USB_CONNECT_H_

//Функция сбора uint16_t из двух его байтов
uint16_t bts_to_sh(char major, char minor);

//Функция сбора сообщения из массива байт
void Glue_Income_Message(uint8_t *income_message, uint16_t *out_message, uint16_t size);

//Функция проверки массива на пустоту
uint8_t isEmptyBuffer(uint8_t* buf, uint32_t count);

extern int8_t CDC_Receive_FS(uint8_t* Buf, uint32_t *Len);

//Функция приема (почти то же самое, что и по прерыванию, но работает только по вызову и с заносом в массив)
uint8_t Recieve_Message(uint8_t *income_message, uint32_t size, uint16_t Timeout);

//Функция приема всего что отправят
uint8_t RecieveAll(uint8_t *income_message, uint16_t Timeout);

//Для работы с ЦАП AD9744. Прием пакетами и работа с DMA
uint8_t SHIND_Recieve_Signal_and_Run(uint32_t size, uint16_t Timeout, DMA_HandleTypeDef *hdma, uint32_t DstAddress);

//Функция передачи (обычная функция передачи работает максимум на 2048 байт)
uint8_t Transmit_Message(uint8_t *out_message, uint32_t size);

#endif /* INC_USB_CONNECT_H_ */
