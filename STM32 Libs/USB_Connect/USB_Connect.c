/*
 * USB_Connect.c
 *
 *  Created on: Oct 13, 2020
 *      Author: senya
 */
#include "USB_Connect.h"

//Функция сбора uint16_t из двух его байтов
uint16_t bts_to_sh(char major, char minor) {
	return ((major<<8)+minor);
}

//Функция сбора сообщения из массива байт
void Glue_Income_Message(uint8_t *income_message, uint16_t *out_message, uint16_t size) {
	for(uint16_t i=0;i<size;i++) {
		out_message[i]=bts_to_sh(income_message[2*i], income_message[2*i+1]);
	}
}

//Функция приема (почти то же самое, что и по прерыванию, но работает только по вызову и с заносом в массив)
/*uint8_t Recieve_Message(uint8_t *income_message, uint32_t size, uint16_t Timeout) {
	//Переменные для таймаута
	uint32_t tickstart = HAL_GetTick();
	uint32_t tickstop = tickstart+Timeout;

	USBD_CDC_HandleTypeDef *hcdc = (USBD_CDC_HandleTypeDef *)&hUsbDeviceFS.pClassData;
	hcdc->RxBuffer = income_message;

	if(hUsbDeviceFS.pClassData != NULL) {
		HAL_PCD_EP_Receive(hUsbDeviceFS.pData, CDC_OUT_EP, (uint8_t*)&income_message[0], size);
	}

	//Пока время начала меньше времени конца ждем приема
	while(tickstart<tickstop) {
		//Ниже написан очень интересный цикл, потому что если запустить функцию
		//HAL_PCD_EP_Receive без условия (hUsbDeviceFS.pClassData == NULL), то он улетит в
		//Hard_Fault, но при этом прием идет только когда (hUsbDeviceFS.pClassData != NULL)
		while (hUsbDeviceFS.pClassData == NULL)
			{
				if(hUsbDeviceFS.pClassData != NULL) {
					HAL_PCD_EP_Receive(hUsbDeviceFS.pData, CDC_OUT_EP, (uint8_t*)&income_message[0], size);
				}
				tickstart = HAL_GetTick();

				if(tickstart>tickstop) {
					return 1;
				}
			}

		//Подтираем ненужные значения
		if(((uint32_t*)income_message[size-4] != NULL)&&(size > 4)) {
			uint32_t in_size=strlen(income_message);
			if(size < in_size) {
				for(uint32_t t=size; t<in_size; t++) {
					income_message[t]=NULL;
				}
			}

			PCD_HandleTypeDef *hpcd = &hUsbDeviceFS.pData;
			hpcd->Init.dma_enable = 0;

			//Если сообщение пришло, возвращаем 0
			return 0;
		}

		//Переприсваиваем время
		tickstart = HAL_GetTick();
	}
	//Если время прошло, то возвращаем 1
	return 1;
}*/

uint8_t received = 0;
uint8_t *buf;
uint32_t len;

int8_t CDC_Receive_FS(uint8_t* Buf, uint32_t *Len)
{
  /* USER CODE BEGIN 6 */
//	if(received == 0) {
		USBD_CDC_SetRxBuffer(&hUsbDeviceFS, &Buf[0]);
		USBD_CDC_ReceivePacket(&hUsbDeviceFS);
		buf = Buf;
		len = *Len;
		received = 1;
//		memcpy(addr_buf, buf, len);
//	}

  return (USBD_OK);
  /* USER CODE END 6 */
}

extern DMA_HandleTypeDef hdma_memtomem_dma1_stream1;

uint8_t Recieve_Message(uint8_t *income_message, uint32_t size, uint16_t Timeout) {
	//Переменные для таймаута
	uint32_t tickstart = HAL_GetTick();
	uint32_t tickstop = tickstart + Timeout;
	uint32_t offset = 0;

//	USBD_CDC_HandleTypeDef *hcdc = (USBD_CDC_HandleTypeDef *)&hUsbDeviceFS.pClassData;
//	hcdc->RxBuffer = income_message;

//	addr_buf = income_message;

	//Пока время начала меньше времени конца ждем приема
	while(tickstart<tickstop) {
//		uint32_t size_buf = USBD_LL_GetRxDataSize(hcdc, income_message);
		if(received == 1) {
			memcpy((uint8_t*)(income_message + offset), (uint8_t*)buf, len);
//			HAL_DMA_Start_IT(&hdma_memtomem_dma1_stream1, buf, income_message + offset, len / 2);
			offset += len;
			received = 0;
//			printf("%d", buf[0]);
		}

		if(offset == size) {
			return 0;
		}

		//Переприсваиваем время
		tickstart = HAL_GetTick();
	}
	//Если время прошло, то возвращаем 1
	return 1;
}

//Функция приема всего что отправят
uint8_t RecieveAll(uint8_t *income_message, uint16_t Timeout) {
	//Переменные для таймаута
	uint32_t tickstart = HAL_GetTick();
	uint32_t tickstop = tickstart+Timeout;
	uint16_t size = 0;

	USBD_CDC_HandleTypeDef *hcdc = (USBD_CDC_HandleTypeDef *)&hUsbDeviceFS.pClassData;
	hcdc->RxBuffer = &income_message[0];

	if(hUsbDeviceFS.pClassData != NULL) {
		HAL_PCD_EP_Receive(hUsbDeviceFS.pData, CDC_OUT_EP, &income_message[0], 1);
	}

	//Пока время начала меньше времени конца ждем приема
	while(tickstart<tickstop) {
		while (hUsbDeviceFS.pClassData != NULL)
			{
				if(hUsbDeviceFS.pClassData != NULL) {
					size = HAL_PCD_EP_GetRxCount(hUsbDeviceFS.pData, CDC_OUT_EP);
					HAL_PCD_EP_Receive(hUsbDeviceFS.pData, CDC_OUT_EP, &income_message, size);
				}
				tickstart = HAL_GetTick();

				if(size>0) {
					PCD_HandleTypeDef *hpcd = &hUsbDeviceFS.pData;
					hpcd->Init.dma_enable = 0;

					return 0;
				}

				if(tickstart>tickstop) {
					return 1;
				}
			}
		tickstart = HAL_GetTick();
	}
}

//Для работы с ЦАП AD9744. Прием пакетами и работа с DMA
uint8_t SHIND_Recieve_Signal_and_Run(uint32_t size, uint16_t Timeout, DMA_HandleTypeDef *hdma, uint32_t DstAddress) {
	//Переменные для таймаута
	uint32_t tickstart = HAL_GetTick();
	uint32_t tickstop = tickstart+Timeout;

	//Буфер для приема
	uint8_t income_message[2048] = {0,};
	//Количество принятых байт в данный момент
	uint16_t size_now = 0;
	//Размер принимаемого пакета (половина от того, что отправляется с пк)
	uint32_t size_of_packet = 1024;

	USBD_CDC_HandleTypeDef *hcdc = (USBD_CDC_HandleTypeDef *)&hUsbDeviceFS.pClassData;
	hcdc->RxBuffer = &income_message[0];

	//Пока время начала меньше времени конца ждем приема
	while(tickstart<tickstop) {
		while((hUsbDeviceFS.pClassData != NULL))	{
			if((hUsbDeviceFS.pClassData != NULL)&&(size>size_now)) {
				if(HAL_PCD_EP_Receive(hUsbDeviceFS.pData, CDC_OUT_EP, (uint8_t*)&income_message[0], size_of_packet) == HAL_OK) {
					SHIND_DAC_Start_IT(hdma, &income_message[0], DstAddress, size_of_packet);
					size_now+=size_of_packet;
				}
			}
		}

			if(size<=size_now) {
				return 0;
			}

			tickstart = HAL_GetTick();

			if(tickstart>tickstop) {
				return 1;
			}
	}


}

//Функция передачи (обычная функция передачи работает максимум на 2048 байт)
uint8_t Transmit_Message(uint8_t *out_message, uint32_t size) {
	//Определяем количество отправляемых пакетов
	uint8_t size_pack=size/61440;
	if(size%61440>0) {
		size_pack++;
	}

	  USBD_CDC_HandleTypeDef *hcdc = (USBD_CDC_HandleTypeDef *)&hUsbDeviceFS.pClassData;

	  //Отправляем наши пакеты
	  for(uint16_t i=0; i<size_pack;i++) {
		  if(i!=size_pack-1) {
//			USBD_CDC_SetTxBuffer(&hUsbDeviceFS, &out_message[0]+(i*61440), 61440);
//			USBD_CDC_TransmitPacket(&hUsbDeviceFS);
//			CDC_Transmit_FS(&out_message[0]+(i*61440), 61440);
			hcdc->TxBuffer = &out_message[0]+(i*61440);
			hcdc->TxLength = 61440;
//			(void)USBD_LL_Transmit(&hUsbDeviceFS, CDC_IN_EP, &out_message[0]+(i*61440), 61440);
			HAL_PCD_EP_Transmit(hUsbDeviceFS.pData, CDC_IN_EP, &out_message[0]+(i*61440), 61440);

			//Время для полной отправки посылки
			HAL_Delay(100);
		} else {
//			USBD_CDC_SetTxBuffer(&hUsbDeviceFS, &out_message[0]+(i*61440), size-(i*61440));
//			USBD_CDC_TransmitPacket(&hUsbDeviceFS);
			//CDC_Transmit_FS(&out_message[0]+(i*61440), size-(i*61440));
			hcdc->TxBuffer = &out_message[0]+(i*61440);
			hcdc->TxLength = size-(i*61440);
//			(void)USBD_LL_Transmit(&hUsbDeviceFS, CDC_IN_EP, &out_message[0]+(i*61440), size-(i*61440));
			HAL_PCD_EP_Transmit(hUsbDeviceFS.pData,CDC_IN_EP, &out_message[0]+(i*61440), size-(i*61440));
		}
	}
}
