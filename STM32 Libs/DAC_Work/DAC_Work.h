/*
 * DAC_Work.h
 *
 *  Created on: Dec 3, 2020
 *      Author: user
 */
#include "main.h"
#include "usb_device.h"

#ifndef INC_DAC_WORK_H_
#define INC_DAC_WORK_H_

#define SIZE_OF_DAC 65535

void DAC_Start_DMA(DAC_HandleTypeDef *hdac, uint32_t Channel, uint32_t *sin_mas, uint32_t cnt, uint32_t Alignment, TIM_HandleTypeDef *htim);

#endif /* INC_DAC_WORK_H_ */
