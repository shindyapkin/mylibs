/*
 * DAC_Work.c
 *
 *  Created on: Dec 3, 2020
 *      Author: user
 */

#include "DAC_Work.h"

//Количество сдвигов в коллбеке
uint8_t DAC_Callback_Count = 0;

//Счетчик коллбеков
uint8_t Callback_Counter = 0;

//Адрес массива синуса
uint32_t Buf_Addr;

//Остаток для отправления
uint32_t rest = 0;


//В этом коллбеке будем перемещать адрес, если размер массива выше 2^16 Байт
void DAC_DMAConvCpltCh1(DMA_HandleTypeDef *hdma)
{
  DAC_HandleTypeDef *hdac = (DAC_HandleTypeDef *)((DMA_HandleTypeDef *)hdma)->Parent;

  HAL_DAC_Stop_DMA(hdac, DAC_CHANNEL_1);

  if(Callback_Counter==DAC_Callback_Count) {
	  Callback_Counter=0;
	  DAC_Callback_Count=0;
//	  HAL_DAC_Start_DMA(hdac, DAC_CHANNEL_1, Buf_Addr, SIZE_OF_DAC, DAC_ALIGN_12B_R);
  } else {
	  if(Callback_Counter<(DAC_Callback_Count-1)) {
		  HAL_DAC_Start_DMA(hdac, DAC_CHANNEL_1, Buf_Addr+2*SIZE_OF_DAC*Callback_Counter, SIZE_OF_DAC, DAC_ALIGN_12B_R);
	  }
	  if(Callback_Counter==(DAC_Callback_Count-1)) {
		  HAL_DAC_Start_DMA(hdac, DAC_CHANNEL_1, Buf_Addr+2*SIZE_OF_DAC*Callback_Counter, rest, DAC_ALIGN_12B_R);
	  }
	  Callback_Counter++;
  }

#if (USE_HAL_DAC_REGISTER_CALLBACKS == 1)
  hdac->ConvCpltCallbackCh1(hdac);
#else
  HAL_DAC_ConvCpltCallbackCh1(hdac);
#endif /* USE_HAL_DAC_REGISTER_CALLBACKS */

  hdac->State = HAL_DAC_STATE_READY;
}


//Функция для генерации сигнала, где буфер размером выше 2^16 Байт
void DAC_Start_DMA(DAC_HandleTypeDef *hdac, uint32_t Channel, uint32_t *sin_mas, uint32_t cnt, uint32_t Alignment, TIM_HandleTypeDef *htim) {
	//Считаем количество коллбеков
	DAC_Callback_Count = cnt/SIZE_OF_DAC;
	if(cnt%SIZE_OF_DAC>0) {
		DAC_Callback_Count++;
	}

	//Обнулим глобальные переменные
	Callback_Counter = 0;

	Buf_Addr = &sin_mas[0];

	HAL_TIM_Base_Start(htim);

	//Если размер массива выше 2^16, то отправляем 65535 байт
	if(DAC_Callback_Count>1) {
		rest = cnt - (SIZE_OF_DAC*(DAC_Callback_Count-1));
		HAL_DAC_Start_DMA(hdac, Channel, Buf_Addr, SIZE_OF_DAC-10, Alignment);
	} else {
		HAL_DAC_Start_DMA(hdac, Channel, Buf_Addr, cnt, Alignment);
	}
	Callback_Counter++;
}
