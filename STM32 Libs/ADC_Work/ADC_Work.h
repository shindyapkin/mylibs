/*
 * ADC_Work.h
 *
 *  Created on: Nov 11, 2020
 *      Author: user
 */

#include "main.h"
#include "stm32h7xx_it.h"

extern DMA_HandleTypeDef hdma_adc1;

typedef enum
{
  BUF_NO_BUILDED = 0,
  BUF_BUILDED = 1,
}ADC_BUF_STATE;


//Глобальная переменная обозначающая завершение сборки буфера
uint8_t ADC_Buf_State;

#ifndef INC_ADC_WORK_H_
#define INC_ADC_WORK_H_

//Функция работы заполнения буфера АЦП
void ADC_Start_DMA(ADC_HandleTypeDef *hadc, uint32_t *pData, uint32_t Length);

//Функция ожидания конца работы АЦП
uint8_t ADC_WaitForCmplt();

#endif /* INC_ADC_WORK_H_ */
