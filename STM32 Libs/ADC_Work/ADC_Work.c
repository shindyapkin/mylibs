/*
 * ADC_Work.c
 *
 *  Created on: Nov 11, 2020
 *      Author: senya
 */

#include "ADC_Work.h"

//Т.к. АЦП уродский и показывает, что длина его буфера uint32_t, а по факту он uint16_t, поэтому обрабатывать будем кусками
//Количество запусков АЦП
uint16_t ADC_Packs=0;

//Счетчик запусков АЦП
uint16_t ADC_Counter=0;

//Общая длина буфера АЦП
uint32_t ADC_Total_Len=0;

//Длина массива для заноса в АЦП
uint16_t ADC_Buf_Len=65535;

//Адрес первого байта данных
uint32_t *Data_Addr;

//Для параметров АЦП передающихся в функции ADC_Start_DMA
ADC_HandleTypeDef *hadc_w;

uint8_t Iter_Count=0;

//Функция работы заполнения буфера АЦП
void ADC_Start_DMA(ADC_HandleTypeDef *hadc, uint32_t *pData, uint32_t Length) {

	//Обнуляем состояние сборки буфера
	ADC_Buf_State=BUF_NO_BUILDED;

	//Присвоим глобальному значению параметры АЦП
	hadc_w=(ADC_HandleTypeDef*)hadc;

	//Адрес начала буфера АЦП
	Data_Addr=&pData[0];

	//Присвоим значение общей длины буфера АЦП
	ADC_Total_Len=Length;

	//Подсчитаем количество запусков АЦП
	ADC_Packs=Length/ADC_Buf_Len;
		if(Length%ADC_Buf_Len>0) {
			ADC_Packs++;
		}

	if(ADC_Packs==1) {
		//Если размер буфера должен быть меньше 64кБайт, то просто запускаем АЦП
		HAL_ADC_Start_DMA(hadc_w, (uint16_t*)Data_Addr, Length);

		//Сообщаем что сборка буфера завершена
		ADC_Buf_State=BUF_BUILDED;

		//Обнулим колиество запусков
		ADC_Packs=0;
	} else {
		//Если размер больше 64кБайт, то занесем его в несколько этапов
		HAL_ADC_Start_DMA(hadc_w, (uint16_t*)Data_Addr, ADC_Buf_Len);

		ADC_Counter++;
	}

}

//Функция ожидания конца работы АЦП
uint8_t ADC_WaitForCmplt() {
	return ADC_Buf_State;
}

//Нужен если буфер больше 64кБайт и его надо продолжать заполнять
extern void DMA1_Stream0_IRQHandler(void)
{

	HAL_DMA_IRQHandler(&hdma_adc1);

	//Первое прерывание идет на полбуфера. Пропускаем его. На второе запускаем обработку дальше
	if(Iter_Count==1) {

		//Если нужно заполнять в несколько этапов, то идем по условиям
		if(ADC_Packs>0) {
			HAL_ADC_Stop_DMA(hadc_w);

			//Пока мы не дошли до последней цикла создания буффера АЦП, заносим туда по 64кБайта
			if(ADC_Counter<(ADC_Packs-1)) {
				HAL_ADC_Start_DMA(hadc_w, (uint16_t*)Data_Addr+ADC_Buf_Len*ADC_Counter, ADC_Buf_Len);

				ADC_Counter++;
			} else {
			//В самом последнем куске буфера, отправляем остатки
				HAL_ADC_Start_DMA(hadc_w, (uint16_t*)Data_Addr+ADC_Buf_Len*ADC_Counter, ADC_Total_Len-ADC_Buf_Len*ADC_Counter);

				//Сообщаем что сборка буфера завершена
				ADC_Buf_State=BUF_BUILDED;

				//Т.к. это конечная точка, то обнуляем все глобальные переменные
				//Количество запусков АЦП
				ADC_Packs=0;
				//Счетчик запусков АЦП
				ADC_Counter=0;
				//Общая длина буфера АЦП
				ADC_Total_Len=0;
			}
		}
		//Обнулим счетчик
		Iter_Count--;
	}else {
		Iter_Count++;
	}

}
