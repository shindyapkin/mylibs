/*
 * DAC_AD9744_Work.c
 *
 *  Created on: Jan 21, 2021
 *      Author: shindyapkin
 */

#include "DAC_AD9744_work.h"

uint16_t SrcAddress[200]={0,};

//Функция инициализации необходимого таймера и запуска DMA
void SHIND_InitDMA(TIM_HandleTypeDef* htim, uint32_t CLK_CHANNEL, uint16_t mode) {
	  __HAL_TIM_ENABLE_DMA(htim, mode);
	  __HAL_TIM_ENABLE(htim);
	  HAL_TIM_PWM_Start(htim, CLK_CHANNEL);
}

//Функция переключения состояния пина
void SHIND_TogglePin(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin) {
	if(GPIOx->ODR) {
		GPIOx->ODR = !GPIO_Pin;
	} else {
		GPIOx->ODR = GPIO_Pin;
	}
}

//Функция установки состояния пина
void SHIND_SetPin(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin, GPIO_PinState PinState) {
	if(PinState) {
		GPIOx->ODR = GPIO_Pin;
	} else {
		GPIOx->ODR = !GPIO_Pin;
	}
}

//Функция для становки значения ЦАП на одном регистре
void SHIND_DAC_SetNumOR(GPIO_TypeDef *GPIOx, uint16_t num) {
	GPIOx->ODR = num;
	GPIOx->ODR = 32768;
}

//Запускает массив данных в ЦАП. Работает через DMA
void SHIND_DAC_Start_IT(DMA_HandleTypeDef *hdma, uint32_t SrcAddress, uint32_t DstAddress, uint32_t DataLength) {
	HAL_DMA_Start_IT(hdma, SrcAddress, DstAddress , DataLength);
}

//Функция генерации синусоиды определенной частоты (кГц)
void SHIND_DAC_StartSin(DMA_HandleTypeDef *hdma, TIM_TypeDef* TIM, uint32_t DstAddress, uint16_t Freq_kHz) {
	uint16_t DataLength = 0;
	if(Freq_kHz<=400) {
		DataLength = 100;
		TIM->ARR = 250-1; //(6*Freq_kHz)/400-1;
	}

	if(DataLength>0) {
		for(uint16_t i = 0; i<DataLength; i+=2) {
			  float f = sin(6.18*i*1/DataLength)+1;
			  SrcAddress[i]=8192*f;
			  SrcAddress[i+1]=32768+SrcAddress[i];
		}

		SHIND_DAC_Start_IT(hdma, SrcAddress, DstAddress, DataLength);
	}
}
