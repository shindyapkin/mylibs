/*
 * DAC_AD9744_Work.h
 *
 *  Created on: Jan 21, 2021
 *      Author: shindyapkin
 */

//.........................................!!!ВАЖНО!!!.........................................//
		//Для работы данной библиотеки необходимо:
			//*все пины, подключаемые к ЦАПу, должны находиться в одном регистре в порядке пинов: 0-13
				//(такой порядок подошел STM32H743ZI NUCLEO для регистра B);
			//*настроить таймер на Prescaler 1 и Counter Period 3 (максимальная скорость);
			//*включить PWM канал (необходимо для ножки тактирования СLK), поставить значения:
				//Pulse - 2 (или половина от Counter Period + 1);
				//Fast Mode - Enable;
				//CH Polarity - Low.
			//*настроить DMA для таймера TIMx_UP и режим[ОПТИМАЛЬНО] Circular mode. Длина данных Half Word;
			//*в разделе NVIC отключить IRQ и HAL handler'ы для настроенных таймера и DMA.
//.........................................!!!ВАЖНО!!!.........................................//

#include "main.h"


#ifndef INC_DAC_AD9744_WORK_H_
#define INC_DAC_AD9744_WORK_H_

//Функция инициализации необходимого таймера и запуска DMA
//Пример: SHIND_InitDMA(&htim17, TIM_CHANNEL_1, TIM_DMA_UPDATE);
void SHIND_InitDMA(TIM_HandleTypeDef* htim, uint32_t CLK_CHANNEL, uint16_t mode);

//Функция переключения состояния пина
void SHIND_TogglePin(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin);

//Функция установки состояния пина
void SHIND_SetPin(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin, GPIO_PinState PinState);

//Функция для работы ЦАП на одном регистре
	//num - необходимое значение, которое необходимо выдать в ЦАПе
void SHIND_DAC_SetNumOR(GPIO_TypeDef *GPIOx, uint16_t num);

//Запускает массив данных в ЦАП. Работает через DMA.
	//В hdma передаем что-то вроде &hdma_tim17_up
	//SrcAddress - массив данных, которые мы будем передавать в регистр.
	//DstAddress - адрес регистра, в которое мы будем передавать данные.
		//Пример: (uint32_t)&GPIOB->ODR
	//DataLenght - длина передаваемых данных (не в байтах, а в количестве элементов)
	//Пример вызова функции: SHIND_DAC_Start_IT(&hdma_tim17_up, (uint32_t)t, (uint32_t)&GPIOB->ODR , dot_cnt);
void SHIND_DAC_Start_IT(DMA_HandleTypeDef *hdma, uint32_t SrcAddress, uint32_t DstAddress, uint32_t DataLength);

//Функция генерации синусоиды определенной частоты (кГц)
//ФУНКЦИЯ НЕ ДОДЕЛАНА
void SHIND_DAC_StartSin(DMA_HandleTypeDef *hdma, TIM_TypeDef* TIM, uint32_t DstAddress, uint16_t Freq_kHz);


#endif /* INC_DAC_AD9744_WORK_H_ */
