/*
 * PWM_Control.c
 *
 *  Created on: Oct 13, 2020
 *      Author: senya
 */
#include "PWM_Control.h"

extern TIM_HandleTypeDef htim17;
extern TIM_HandleTypeDef htim1;

//Сделаем наши параметры глобальными, чтобы не мучаться в прерывании
TIM_HandleTypeDef *htim_pwm;
TIM_HandleTypeDef *htim_it;
uint32_t Channel;
uint16_t time;
int cnt=0;

//Функция старта ШИМ на определенный промежуток времени. Отключение шим будет находиться в вызове прервывания таймера. Время в мкс
//Сначала задаем таймер на ШИМ, потом таймер на счет времени генерации, канал таймера ШИМ и время генерации
void PWM_Start_Delay(TIM_HandleTypeDef *htim_pwm1, TIM_HandleTypeDef *htim_it1, uint32_t Channel1, uint16_t time1) {
	htim_pwm=(TIM_HandleTypeDef*)htim_pwm1;
	htim_it=(TIM_HandleTypeDef*)htim_it1;
	Channel=(uint32_t)Channel1;
	time=time1;

	TIMER_IT->ARR = time-1;
	HAL_TIM_Base_Start_IT(htim_it);
	HAL_TIM_PWM_Start(htim_pwm, Channel);
	HAL_TIMEx_PWMN_Start(htim_pwm, Channel);
}

//Прерывание по необходимому таймеру (лучше всего использовать TIM17)
//Настроить таймер по микросекундному счету
//Если используется другой таймер, то необходимо сменить название
//Из файла .c с прерываниями удалить нужную функцию, чтобы не дублировались
void TIM17_IRQHandler(void) {

	//По прерывнию отключаем ШИМ
	if(cnt==1) {
		TIMER_PWM->CCR1 = 0;

		//Выключаем генерацию ШИМ'ов и таймер прерывания
		HAL_TIM_Base_Stop_IT(htim_it);
		HAL_TIMEx_PWMN_Stop(htim_pwm, Channel);
		HAL_TIM_PWM_Stop(htim_pwm, Channel);

		//Уродство, чтобы успеть сменить скважность. Увеличивает длительность импульса
		//for(uint8_t i=1; i<10; i++) {}

		//Выключаем генерацию ШИМ'ов и таймер прерывания повторно (выше шанс выключить в нужный момент)
		//HAL_TIM_PWM_Stop(htim_pwm, Channel);
		//HAL_TIMEx_PWMN_Stop(htim_pwm, Channel);


	}

	if (cnt<1) {
		cnt+=1;
	}

	HAL_TIM_IRQHandler(htim_it);
}
